import { Routes, RouterModule } from '@angular/router';

import { AuthGuard } from '../guard/auth.guard';
import { Constants } from '../utils/constants';

import { ManagerComponent, NewUserComponent } from '../components/admin';
import { LoginComponent } from '../components/login/login.component';
import { ProfileComponent } from '../components/profile/profile.component';

const routes: Routes = [
    {
        path: '',
        redirectTo: Constants.PROFILE_PATH,
        pathMatch: 'full'
    },
    {
        path: Constants.MANAGER_PATH,
        component: ManagerComponent,
        canActivate: [AuthGuard]
    },
    {
        path: Constants.LOGIN_PATH,
        component: LoginComponent,
        canActivate: [AuthGuard]
    }, {
        path: Constants.PROFILE_PATH,
        component: ProfileComponent,
        canActivate: [AuthGuard]
    }, {
        path: Constants.NEW_USER_PATH,
        component: NewUserComponent,
        canActivate: [AuthGuard]
    }, {
        path: '**',
        redirectTo: Constants.LOGIN_PATH,
        pathMatch: 'full'
    }
];

export const Routing = RouterModule.forRoot(routes);
