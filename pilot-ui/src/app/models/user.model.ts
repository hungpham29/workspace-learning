export class User {
    id: number;
    username: string;
    password: string;
    roles: string[];
    status: boolean;
    firstname: string;
    lastname: string;
    email: string;
    birthday: Date;
    gender: string;
    address: string;
    avatar: string;
    lastUpdateTs: Date;
    lastUpdateBy: string;
    phone: string;
}
