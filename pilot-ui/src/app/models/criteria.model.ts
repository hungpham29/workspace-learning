export class Criteria {
    page: number;
    size: number;
    username: string;
    firstname: string;
    lastname: string;
    email: string;
    gender: string;
    createDateFrom: Date;
    createDateTo: Date;
    status: boolean;
}
