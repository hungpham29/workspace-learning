import { Component, OnInit } from '@angular/core';
import { Router, NavigationStart } from '@angular/router';

import _ from 'lodash';

import { AuthenticationService } from './services/authentication.service';

import { Constants } from './utils/constants';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html'
})
export class AppComponent implements OnInit {

    constructor(private authentication: AuthenticationService, private router: Router) { }

    ngOnInit(): void {

        this.router.events.subscribe(event => {
            if (event instanceof NavigationStart && _.trim(event.url).substring(1) !== Constants.LOGIN_PATH) {
                this.authentication.getCurrentUser().subscribe(
                    value => { }
                );
            }
        });
    }

}
