export { ManagerComponent } from './manager/admin.manager.component';
export { ManagerService } from './manager/admin.manager.service';
export { NewUserComponent } from './new-user/new-user.component';
export { NewUserService } from './new-user/new-user.service';
