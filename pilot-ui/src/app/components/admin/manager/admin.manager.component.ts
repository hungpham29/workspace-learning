import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import _ from 'lodash';

import { ConfirmationService } from 'primeng/components/common/api';
import { MessageService } from 'primeng/components/common/messageservice';

import { ManagerService } from './admin.manager.service';

import { Constants } from '../../../utils/constants';
import { Criteria } from '../../../models/criteria.model';

@Component({
    selector: 'app-manager',
    templateUrl: './admin.manager.component.html'
})

export class ManagerComponent implements OnInit {

    dataResource: Object[];
    totalRecords: number;
    moreSearchCriteria: boolean;
    criteria: Criteria = new Criteria();
    deleteAll: boolean;

    /**
     * Constructor.
     * @param router The router.
     * @param managerService The manager service to call api.
     * @param confirmationService The confirm popup service.
     * @param messageService The message service.
     */
    constructor(private router: Router, private managerService: ManagerService,
        private confirmationService: ConfirmationService, private messageService: MessageService) { }

    /**
     * Init variable.
     */
    ngOnInit() {

        this.totalRecords = 0;
        this.moreSearchCriteria = false;
        this.deleteAll = false;
        this.criteria.page = 0;
        this.criteria.size = 5;
    }

    /**
     * Load user for datatable.
     * @param event The information to load data.
     */
    loadUser(event: any) {

        this.deleteAll = false;

        if (event && !_.isNil(event.first) && !_.isNil(event.rows)) {
            this.criteria.page = event.first / event.rows;
        }

        this.managerService.searchWithCriteria(this.criteria).subscribe(
            values => {
                this.dataResource = values['content'];
                this.totalRecords = values['totalElements'];
            });
    }

    /**
     * Toggle search criteria.
     */
    toggleMoreSearchCrieria() {
        this.moreSearchCriteria = !this.moreSearchCriteria;
    }

    /**
     * Search user by condition.
     */
    search() {

        this.criteria.page = 0;
        this.criteria.size = 5;

        if (!this.moreSearchCriteria) {
            this.criteria.gender = null;
            this.criteria.createDateFrom = null;
            this.criteria.createDateTo = null;
        }

        this.loadUser(this.criteria);
    }

    /**
     * Reset all condition.
     */
    resetCriteria() {
        this.criteria = new Criteria();
        this.criteria.page = 0;
        this.criteria.size = 5;

        this.loadUser(this.criteria);
    }

    /**
     * Handle click check all.
     */
    toggleCheckboxDeleteAll() {
        if (this.dataResource) {
            for (const row of this.dataResource) {
                if (!this.checkDefaultAdmin(row['username'])) {
                    row['delete'] = this.deleteAll;
                }
            }
        }
    }

    /**
     * Handle click checkbox.
     */
    toggleCheckboxDelete() {
        const _this = this;
        if (this.dataResource) {
            const countTotalAccout = _.filter(this.dataResource, function (user) {
                return !_this.checkDefaultAdmin(user['username']);
            }).length;

            this.deleteAll = _.filter(this.dataResource, function (user) {
                return user['delete'] && !_this.checkDefaultAdmin(user['username']);
            }).length === countTotalAccout;
        }
    }

    /**
     * Delete multi user.
     */
    deleteMulti(resultTable: any) {
        const accountDelete = [];

        for (const account of this.dataResource) {
            if (account['delete']) {
                accountDelete.push(account);
            }
        }

        if (accountDelete.length > 0) {
            const ids = _.map(accountDelete, 'id');

            this.confirmationService.confirm({
                key: 'confirmDeleteUser',
                message: `Are you sure that you want to delete ${ids.length > 1 ? 'these' : 'this'}  account?`,
                accept: () => {
                    this.managerService.deleteMultiUser(ids).subscribe(
                        value => {
                            if (value) {
                                resultTable.reset();
                                this.messageService.add({
                                    severity: 'success',
                                    summary: 'Information',
                                    detail: `Account${ids.length > 1 ? 's' : ''} is deleted successfuly.`
                                });
                            }
                        },
                        error => { });
                }
            });
        }
    }

    /**
     * Delete user.
     */
    delete(user: Object, resultTable: any) {
        if (!_.isNil(user)) {
            this.confirmationService.confirm({
                key: 'confirmDeleteUser',
                message: `Are you sure that you want to delete "${user['username']}" account?`,
                accept: () => {
                    this.managerService.deleteUser(user).subscribe(
                        value => {
                            if (value) {
                                resultTable.reset();
                                this.messageService.add({
                                    severity: 'success',
                                    summary: 'Information',
                                    detail: `Account ${user['username']} is deleted successfuly.`
                                });
                            }
                        },
                        error => { });
                }
            });
        }
    }

    /**
     * Update status.
     */
    updateStatus(user: Object, resultTable: any) {
        this.managerService.updateStatus(user).subscribe(
            value => {
                if (!value) {
                    resultTable.reset();
                } else {
                    this.messageService.add({
                        severity: 'success',
                        summary: 'Information',
                        detail: `Account ${user['username']} is changed status successfuly.`
                    });
                }
            },
            error => { resultTable.reset(); });
    }

    /**
     * Check user is default admin account.
     * @param username username's user.
     */
    checkDefaultAdmin(username: string) {
        return username === Constants.DEFAULT_ADMIN_ACC;
    }

    /**
     * Redirect to add new user component.
     */
    redirectToAddNewUser() {
        this.router.navigate([Constants.NEW_USER_PATH]);
    }

}
