import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/Observable';

import { APIService } from '../../../services/api.service';

import { Constants } from '../../../utils/constants';

import { Criteria } from '../../../models/criteria.model';

@Injectable()
export class ManagerService {

    constructor(private api: APIService) { }

    /**
     * Search users have condition map criteria.
     * @param criteria criteria data.
     */
    searchWithCriteria(criteria: Criteria): Observable<Object> {
        return this.api.callAPIPOST(Constants.API_SEARCH_CRITERIA, criteria);
    }

    /**
     * Delete multi user.
     * @param ids ID of users to delete.
     */
    deleteMultiUser(ids: any): Observable<Object> {
        return this.api.callAPIPOST(Constants.API_DELETE_MULTI_USER, ids);
    }

    /**
     * Delete user.
     * @param user user to delete.
     */
    deleteUser(user: Object): Observable<Object> {
        return this.api.callAPIPOST(Constants.API_DELETE_USER, user);
    }

    /**
     * Update status for user.
     * @param user user to update status.
     */
    updateStatus(user: Object): Observable<Object> {
        return this.api.callAPIPOST(Constants.API_UPDATE_STATUS, user);
    }
}
