import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/Observable';

import { APIService } from '../../../services/api.service';

import { User } from '../../../models/user.model';

import { Constants } from '../../../utils/constants';

@Injectable()
export class NewUserService {

    constructor(private api: APIService) { }

    createUser(user: any): Observable<Object> {
        return this.api.callAPIPOST(Constants.API_CREATE_USER, user);
    }
}
