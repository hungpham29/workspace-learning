import { Component, OnInit } from '@angular/core';
import { URLSearchParams } from '@angular/http';

import { MessageService } from 'primeng/components/common/messageservice';

import { NewUserService } from './new-user.service';

import { User } from './../../../models/user.model';

import { Constants } from './../../../utils/constants';
import { Router } from '@angular/router';

@Component({
    selector: 'app-new-user',
    templateUrl: './new-user.component.html'
})

export class NewUserComponent implements OnInit {

    roles: Object[];
    user: User;

    constructor(private newUserService: NewUserService, private router: Router,
        private messageService: MessageService) { }

    ngOnInit() {
        this.user = new User();
        this.user.gender = 'MALE';

        this.roles = [
            {
                value: Constants.ROLE_ADMIN.replace(/ROLE_/gi, ''),
                label: 'Administrator'
            },
            {
                value: Constants.ROLE_MEM.replace(/ROLE_/gi, ''),
                label: 'Member'
            },
            {
                value: Constants.ROLE_SUPRT.replace(/ROLE_/gi, ''),
                label: 'Support'
            }
        ];
    }

    /**
     * Create new user.
     */
    create() {
        this.newUserService.createUser(this.user).subscribe(
            value => {
                if (value) {
                    this.messageService.add({
                        severity: 'success',
                        summary: 'Information',
                        detail: `Account ${this.user.username} is created successfuly.`
                    });

                    this.router.navigate([Constants.MANAGER_PATH]);
                }
            }
        );
    }

    handleUpload(event) {
        console.log(event.originalEvent[0]);
    }

}
