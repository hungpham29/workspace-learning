import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import * as moment from 'moment/moment';
import _ from 'lodash';

import { APIService } from '../../services/api.service';

import { User } from '../../models/user.model';
import { Constants } from '../../utils/constants';

@Injectable()
export class ProfileService {

    constructor(private api: APIService) { }

    getUserInfo(username: string): Observable<Object> {
        const self = this;

        return this.api.callAPIPOST(Constants.API_GET_USER_DETAILS, username).map(
            (res) => {
                let value = res;

                if (typeof value === 'boolean') {
                    value = false;
                } else {
                    value['birthday'] = self.formatDate(value['birthday']);
                }

                return value;
            }
        );
    }

    updateUserDetails(data: Object): Observable<Object> {
        const self = this;

        return this.api.callAPIPOST(Constants.API_UPDATE_USER_DETAILS, data).map(
            (res) => {
                let value = res;

                if (typeof value === 'boolean') {
                    value = false;
                } else {
                    value['birthday'] = self.formatDate(value['birthday']);
                }

                return value;
            }
        );
    }

    /**
     * Format date.
     * @param date The date to format.
     */
    formatDate(date: any) {
        let value = '';

        if (!_.isNil(date)) {
            value = moment(date).format(Constants.DATE_FORMAT_2);
        }

        return value;
    }
}
