import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

import * as moment from 'moment/moment';
import _ from 'lodash';

import { MessageService } from 'primeng/components/common/messageservice';
import { ProfileService } from './profile.service';

import { User } from '../../models/user.model';
import { Constants } from '../../utils/constants';

import { Helper } from '../../utils/helper';

@Component({
    selector: 'app-profile',
    templateUrl: './profile.component.html'
})

export class ProfileComponent implements OnInit {

    user: Object;
    userEdit: Object;
    editMode: boolean;
    isEditRole: boolean;
    displayPopupChangePass: boolean;
    changePass: Object;

    constructor(private profileService: ProfileService, private messageService: MessageService, private helper: Helper) {
    }

    ngOnInit() {
        this.editMode = false;
        this.isEditRole = false;
        this.displayPopupChangePass = false;
        this.user = {};
        this.userEdit = {};
        this.changePass = {};

        const self = this;

        const u = this.getCurrentUser();

        if (this.helper.isAdmin(u)) {
            this.isEditRole = true;
        }

        if (!_.isNil(u)) {
            this.profileService.getUserInfo(u['username']).subscribe(
                value => {
                    if (value) {
                        this.user = value;
                        this.userEdit = _.clone(value);
                    } else {

                    }
                }
            );
        }
    }

    turnOnEditMode() {
        this.userEdit = _.clone(this.user);
        this.editMode = true;
    }

    turnOffEditMode() {
        this.editMode = false;
    }

    resetData(form: NgForm) {
        this.userEdit = _.clone(this.user);
        form.reset(this.userEdit);
    }

    edit() {
        this.profileService.updateUserDetails(this.userEdit).subscribe(
            value => {
                if (value) {
                    this.user = value;
                    this.editMode = false;
                    this.messageService.add({
                        severity: 'success',
                        summary: 'Information',
                        detail: `Your profile is updated successfuly.`
                    });
                }
            }
        );
    }

    showDialogChangePass(form: NgForm) {
        this.changePass = {};
        form.reset(this.changePass);
        this.displayPopupChangePass = true;
    }

    updatePass() {
        console.log(this.changePass);
    }

    /**
     * Get current user.
     */
    private getCurrentUser() {
        let user: Object = null;

        if (!_.isEmpty(localStorage.getItem('currentUser'))) {
            user = JSON.parse(localStorage.getItem('currentUser'));
        }

        return user;
    }

}
