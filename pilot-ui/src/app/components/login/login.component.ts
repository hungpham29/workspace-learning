import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';

import _ from 'lodash';

import { AuthenticationService } from '../../services/authentication.service';

import { Constants } from './../../utils/constants';
import { Helper } from './../../utils/helper';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit, AfterViewInit {

    model: any = { username: 'admin', password: '123456' };

    errors: any = {
        username: false,
        password: false
    };

    error: string;

    @ViewChild('formLogin') form;

    constructor(private router: Router, private authenticationService: AuthenticationService,
        private helper: Helper) { }

    ngOnInit() {
    }

    ngAfterViewInit() {
        this.validateLoginForm();
    }

    /**
     * Validate for login form.
     */
    validateLoginForm() {
        this.form.control.valueChanges
            .subscribe(values => {
                for (const key in values) {
                    if (this.errors.hasOwnProperty(key) && this.form.controls[key].dirty) {
                        this.errors[key] = this.form.controls[key].invalid;
                    }
                }
            });
    }

    /**
     * Handle submit login.
     * @param formLogin login form.
     */
    submit(formLogin: any) {
        if (formLogin.invalid) {
            for (const key in formLogin.controls) {
                if (this.errors.hasOwnProperty(key)) {
                    this.errors[key] = formLogin.controls[key].invalid;
                }
            }
        } else {
            this.authenticationService.login(this.model.username, this.model.password).subscribe(
                result => {
                    if (!result && !_.isNil(this.authenticationService.error)) {
                        this.error = this.authenticationService.error['message'];
                    } else {
                        if (this.helper.isAdmin(this.authenticationService.currentUser)) {
                            this.router.navigate([Constants.MANAGER_PATH]);
                        } else {
                            this.router.navigate([Constants.PROFILE_PATH]);
                        }
                    }
                },
                error => { this.error = 'Have error occur when process login request.'; }
            );
        }
    }

}
