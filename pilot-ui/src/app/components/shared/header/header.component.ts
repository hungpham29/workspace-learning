import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import _ from 'lodash';
import * as moment from 'moment/moment';

import { MenuItem } from 'primeng/components/common/menuitem';

import { AuthenticationService } from './../../../services/authentication.service';
import { Constants } from './../../../utils/constants';


@Component({
    selector: 'app-header',
    templateUrl: './header.component.html'
    // styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

    items: MenuItem[];

    currentUser: Object;

    localTime: any = moment().format('MMMM Do YYYY, h:mm:ss A');

    constructor(private router: Router, private authenticationService: AuthenticationService) { }

    ngOnInit() {

        setInterval(() => this.localTime = moment().format('MMMM Do YYYY, h:mm:ss A'));

        this.items = [
            {
                label: 'Account',
                command: (event) => {
                    this.router.navigate([Constants.PROFILE_PATH]);
                }
            },
            {
                label: 'Logout',
                command: (event) => {
                    this.authenticationService.logout().subscribe(
                        result => {
                            this.router.navigate([Constants.LOGIN_PATH]);
                        }
                    );
                }
            }
        ];
        this.currentUser = this.getCurrentUser();
    }

    /**
     * Get current user.
     */
    private getCurrentUser(): Object {
        let user: Object = null;

        if (!_.isEmpty(localStorage.getItem('currentUser'))) {
            user = JSON.parse(localStorage.getItem('currentUser'));
        }

        return user;
    }

}
