import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import _ from 'lodash';

import { MenuItem } from 'primeng/components/common/menuitem';

import { Helper } from './../../../utils/helper';

@Component({
    selector: 'app-left-menu',
    templateUrl: './left-menu.component.html'
    // styleUrls: ['./header.component.scss']
})

export class LeftMenuComponent implements OnInit {

    private user: Object;

    constructor(public router: Router, private helper: Helper) {
    }

    items: MenuItem[];

    ngOnInit() {
        this.user = this.getCurrentUser();

        if (this.helper.isAdmin(this.user)) {
            this.items = [
                {
                    label: 'User Management',
                    icon: 'fa-users',
                    routerLink: '/admin/manager'
                },
                {
                    label: 'Your Profile',
                    icon: 'fa-user',
                    routerLink: '/profile'
                }
            ];
        } else {
            this.items = [
                {
                    label: 'Your Profile',
                    icon: 'fa-user',
                    routerLink: '/profile'
                }
            ];
        }

    }

    /**
     * Get current user.
     */
    private getCurrentUser(): Object {
        let user: Object = null;

        if (!_.isEmpty(localStorage.getItem('currentUser'))) {
            user = JSON.parse(localStorage.getItem('currentUser'));
        }

        return user;
    }
}
