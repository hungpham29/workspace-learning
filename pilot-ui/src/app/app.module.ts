import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { Routing } from './routes/routing.component';

// PrimeNG
import {
    MenubarModule,
    DropdownModule,
    CheckboxModule,
    ButtonModule,
    TieredMenuModule,
    MenuModule,
    PanelModule,
    RadioButtonModule,
    DataTableModule,
    SharedModule,
    CalendarModule,
    DialogModule,
    ConfirmDialogModule,
    GrowlModule,
    MultiSelectModule,
    TooltipModule,
    FileUploadModule,
    InplaceModule
} from 'primeng/primeng';
import { ConfirmationService } from 'primeng/components/common/api';
import { MessageService } from 'primeng/components/common/messageservice';

import { AuthenticationService, APIService, GlobalErrorHandler } from './services';
import { AuthGuard } from './guard/auth.guard';
import { Helper } from './utils/helper';

import { HeaderComponent, LeftMenuComponent } from './components/shared';
import { LoginComponent } from './components/login/login.component';
import { ProfileComponent, ProfileService } from './components/profile';
import { ManagerComponent, ManagerService, NewUserComponent, NewUserService } from './components/admin';

import { RangeDateValidatorDirective, HighlightDirective, PasswordValidatorDirective } from './directives';

@NgModule({
    declarations: [
        AppComponent,
        HeaderComponent,
        LoginComponent,
        ProfileComponent,
        ManagerComponent,
        LeftMenuComponent,
        NewUserComponent,
        RangeDateValidatorDirective,
        PasswordValidatorDirective,
        HighlightDirective
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        FormsModule,
        HttpModule,
        Routing,
        MenubarModule,
        DropdownModule,
        CheckboxModule,
        ButtonModule,
        TieredMenuModule,
        MenuModule,
        PanelModule,
        RadioButtonModule,
        DataTableModule,
        SharedModule,
        CalendarModule,
        DialogModule,
        ConfirmDialogModule,
        GrowlModule,
        MultiSelectModule,
        TooltipModule,
        FileUploadModule,
        InplaceModule
    ],
    providers: [
        AuthenticationService,
        APIService,
        ManagerService,
        NewUserService,
        ProfileService,
        AuthGuard,
        Helper,
        ConfirmationService,
        MessageService,
        {
            provide: ErrorHandler,
            useClass: GlobalErrorHandler
        }
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
