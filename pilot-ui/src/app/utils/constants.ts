export const Constants = {
  SERVER_PATH: 'http://localhost:8080',
  LOGIN_PATH: 'login',
  PROFILE_PATH: 'profile',
  MANAGER_PATH: 'admin/manager',
  NEW_USER_PATH: 'admin/new-user',

  API_LOGIN: '/api/login',
  API_LOGOUT: '/api/logout',
  API_GET_ALL_USER: '/api/account/all-user',
  API_SEARCH_CRITERIA: '/api/account/search-by-criteria',
  API_UPDATE_STATUS: '/api/account/update-status',
  API_DELETE_USER: '/api/account/delete',
  API_DELETE_MULTI_USER: '/api/account/delete-multi',
  API_GET_CURRENT_USER: '/api/account/current-user',
  API_CREATE_USER: '/api/account/create',
  API_GET_USER_DETAILS: '/api/user-details/user',
  API_UPDATE_USER_DETAILS: '/api/user-details/update-user-details',

  DATE_FORMAT: 'MM/DD/YYYY hh:mm:ss',
  DATE_FORMAT_2: 'MM/DD/YYYY',

  ROLE_ADMIN: 'ROLE_ADMIN',
  ROLE_MEM: 'ROLE_MEM',
  ROLE_SUPRT: 'ROLE_SUPRT',

  DEFAULT_ADMIN_ACC: 'admin'
};
