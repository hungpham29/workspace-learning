import _ from 'lodash';

import { Constants } from './constants';

export class Helper {

    public isAdmin(user: Object) {

        let isAdmin = false;

        if (user && !_.isNil(user['authorities'])) {
            for (const role of user['authorities']) {
                if (role.authority === Constants.ROLE_ADMIN) {
                    isAdmin = true;
                    break;
                }
            }
        }

        return isAdmin;
    }
}
