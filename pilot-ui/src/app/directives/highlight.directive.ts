import { Directive, ElementRef, Renderer2, HostListener, OnInit } from '@angular/core';
import { NgModel } from '@angular/forms';

import _ from 'lodash';

@Directive({
    selector: '[appHighlight]'
})
export class HighlightDirective implements OnInit {

    constructor(private el: ElementRef, private render: Renderer2, private ngModel: NgModel) { }

    ngOnInit() {
        const _self = this;
        this.ngModel.valueChanges.subscribe(value => {
            const parent = this.rowDOM(this.el.nativeElement);

            if (!_.isNil(parent) && parent.tagName === 'TR') {
                _self.toggleClass(parent, value);
            }
        });
    }

    private toggleClass(row, flag) {
        if (flag) {
            this.render.addClass(row, 'row-selected');
        } else {
            this.render.removeClass(row, 'row-selected');
        }
    }

    /**
     * Get tr parent element form node.
     * @param node node to get parent.
     */
    private rowDOM(node) {
        let row = node;

        if (!_.isNil(node)) {
            row = this.render.parentNode(row);

            if (row.tagName !== 'TR' && row.tagName !== 'BODY') {
                row = this.rowDOM(row);
            }
        }

        return row;
    }

    // @HostListener('click')
    // test(event) {
    //     console.log(event)
    //     console.log(this.render.parentNode(this.el.nativeElement));
    // }
}
