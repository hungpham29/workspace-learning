import { Directive, forwardRef, Attribute, ElementRef, Renderer2 } from '@angular/core';
import { NG_VALIDATORS, Validator, AbstractControl } from '@angular/forms';

import _ from 'lodash';

@Directive({
    selector: '[appPasswordValidator][ngModel]',
    providers: [
        { provide: NG_VALIDATORS, useExisting: forwardRef(() => PasswordValidatorDirective), multi: true }
    ]
})

export class PasswordValidatorDirective implements Validator {

    constructor( @Attribute('appPasswordValidator') private appPasswordValidator: string,
        @Attribute('reverse') private reverse) { }

    private get isReverse() {
        return this.reverse === 'true';
    }

    validate(c: AbstractControl): { [key: string]: any; } {
        // self value
        const selfValue = c.value;

        // control vlaue
        const controlValue = c.root.get(this.appPasswordValidator);

        // value not equal
        if (controlValue && selfValue !== controlValue.value && !this.isReverse) {
            return {
                validateEqual: false
            };
        }

        // value equal and reverse
        if (controlValue && selfValue === controlValue.value && this.isReverse) {
            delete controlValue.errors['validateEqual'];
            if (!Object.keys(controlValue.errors).length) {
                controlValue.setErrors(null);
            }
        }

        // value not equal and reverse
        if (controlValue && selfValue !== controlValue.value && this.isReverse) {
            controlValue.setErrors({
                validateEqual: false
            });
        }

        return null;
    }

    // registerOnValidatorChange?(fn: () => void): void {
    //     console.log('asdf')
    // }
}
