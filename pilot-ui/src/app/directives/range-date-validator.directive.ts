import { Directive, forwardRef, Attribute, ElementRef, Renderer2 } from '@angular/core';
import { NG_VALIDATORS, Validator, AbstractControl } from '@angular/forms';

import _ from 'lodash';

@Directive({
    selector: '[appRangeDateValidator][ngModel]',
    providers: [
        { provide: NG_VALIDATORS, useExisting: forwardRef(() => RangeDateValidatorDirective), multi: true }
    ]
})

export class RangeDateValidatorDirective implements Validator {

    constructor( @Attribute('appRangeDateValidator') private appRangeDateValidator: string,
        @Attribute('from') private from) { }

    private get isFrom() {
        return this.from === 'true';
    }

    validate(c: AbstractControl): { [key: string]: any; } {
        const anotherDate = c.root.get(this.appRangeDateValidator);
        let value = null;

        if (!_.isNil(anotherDate)) {

            let from = c.value;
            let to = anotherDate.value;

            if (!this.isFrom) {
                from = anotherDate.value;
                to = c.value;
            }

            if (_.isDate(from) && _.isDate(to)) {
                if (from.getTime() > to.getTime()) {
                    value = {
                        createDateValid: false
                    };
                }
            }

            if (anotherDate.errors) {
                delete anotherDate.errors['createDateValid'];
                if (!Object.keys(anotherDate.errors).length) {
                    anotherDate.setErrors(null);
                }
            }
        }

        return value;
    }

    // registerOnValidatorChange?(fn: () => void): void {
    //     console.log('asdf')
    // }
}
