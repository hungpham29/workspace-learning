import { ErrorHandler, Injectable, Injector } from '@angular/core';
import { Router } from '@angular/router';

import { ConfirmationService } from 'primeng/components/common/api';

import { AuthenticationService } from './authentication.service';
import { Constants } from '../utils/constants';

@Injectable()
export class GlobalErrorHandler implements ErrorHandler {

    constructor(private confirmationService: ConfirmationService, private injector: Injector,
        private authenticationService: AuthenticationService) { }

    public get router(): Router {
        return this.injector.get(Router);
    }

    /**
     * Handle errors.
     * @param error The error.
     */
    handleError(error) {
        if (error && error.status === 401) {
            error = error.json();
            this.confirmationService.confirm({
                key: 'sessionExpired',
                message: error.message,
                accept: () => {
                    this.authenticationService.logout().subscribe(
                        result => {
                            this.router.navigate([Constants.LOGIN_PATH]);
                        }
                    );
                }
            });
        } else if (error && error.status === 504) {
            this.confirmationService.confirm({
                key: 'sessionExpired',
                message: 'App error!',
                accept: () => {
                    this.authenticationService.clearDataStorage();
                    this.router.navigate([Constants.LOGIN_PATH]);
                }
            });
        }
        console.log(error);
        // throw error;
    }
}
