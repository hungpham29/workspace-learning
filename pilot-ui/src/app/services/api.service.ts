import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions, URLSearchParams } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import _ from 'lodash';

import { MessageService } from 'primeng/components/common/messageservice';

import { Constants } from '../utils/constants';

@Injectable()
export class APIService {

    public error: Object;

    constructor(private http: Http, private messageService: MessageService) {
    }

    /**
     * Call api with request method is POST.
     * @param api The URL to call.
     * @param data The parameter for api.
     */
    callAPIPOST(api: string, data: Object, headers: any = null): Observable<Object> {
        this.error = null;

        // if (_.isNil(headers)) {
        //     headers = new Headers({ 'Content-type': 'application/json' });
        // }

        // const options = new RequestOptions({ headers: headers, withCredentials: true });

        return this.http.post(api, data)
            .map((response: Response) => {
                let res: any = true;

                if (response && response.text()) {
                    res = response.json();
                }

                if (res && res.error) {
                    this.error = res;

                    let message = this.error['messages'];

                    if (_.isNil(message)) {
                        message = [this.error['message']];
                    }

                    this.messageService.add({ severity: 'error', summary: 'Error', detail: message });

                    res = false;
                }

                return res;
            });
    }

    /**
     * Call api with request method is GET.
     * @param api The URL to call.
     */
    callAPIGET(api: string): Observable<Object> {
        this.error = null;

        const headers = new Headers({ 'Content-type': 'application/json' });
        const options = new RequestOptions({ headers: headers, withCredentials: true });

        return this.http.get(api, options)
            .map((response: Response) => {
                const res = response.json();

                if (res && res.error) {
                    this.error = res;

                    let message = this.error['messages'];

                    if (_.isNil(message)) {
                        message = [this.error['message']];
                    }

                    this.messageService.add({ severity: 'error', summary: 'Error', detail: message });

                    return false;
                } else {
                    return res || true;
                }
            });
    }

    /**
     * Call api with request method is DELETE.
     * @param api The URL to call.
     */
    callAPIDELETE(api: string): Observable<Object> {
        this.error = null;

        const headers = new Headers({ 'Content-type': 'application/json' });
        const options = new RequestOptions({ headers: headers, withCredentials: true });

        return this.http.delete(api, options)
            .map((response: Response) => {
                const res = response.json();

                if (res && res.error) {
                    this.error = res;

                    let message = this.error['messages'];

                    if (_.isNil(message)) {
                        message = [this.error['message']];
                    }

                    this.messageService.add({ severity: 'error', summary: 'Error', detail: message });

                    return false;
                } else {
                    return res || true;
                }
            });
    }
}
