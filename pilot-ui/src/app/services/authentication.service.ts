import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions, URLSearchParams } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import _ from 'lodash';

import { Constants } from '../utils/constants';


@Injectable()
export class AuthenticationService {

    public currentUser: Object;

    public error: Object;

    constructor(private http: Http) {
    }

    /**
     * Login.
     * @param username The username.
     * @param password The password.
     */
    login(username: string, password: string): Observable<Object> {
        this.error = null;
        this.currentUser = null;
        localStorage.removeItem('currentUser');

        const params = new URLSearchParams();
        params.set('username', username);
        params.set('password', password);

        const headers = new Headers({ 'Content-type': 'application/x-www-form-urlencoded' });
        const options = new RequestOptions({ headers: headers, withCredentials: true });


        return this.http.post(Constants.API_LOGIN, params, options)
            .map((response: Response) => {
                const res = response.json();

                if (_.isObject(res) && _.isNil(res.error)) {
                    this.currentUser = res;
                    localStorage.setItem('currentUser', JSON.stringify(res));

                    return true;
                } else {
                    this.error = res;

                    return false;
                }
            });
    }

    /**
     * Logout.
     */
    logout(): Observable<boolean> {
        return this.http.post(Constants.API_LOGOUT, null)
            .map((response: Response) => {
                this.clearDataStorage();

                return true;
            });
    }

    /**
     * Clear data storaged.
     */
    clearDataStorage() {
        this.error = null;
        this.currentUser = null;
        localStorage.removeItem('currentUser');
    }

    /**
     * Get current user.
     */
    getCurrentUser(): Observable<Object> {
        return this.http.post(Constants.API_GET_CURRENT_USER, null)
            .map((response: Response) => {
                return response.json();
            });
    }
}
