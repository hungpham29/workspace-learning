import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { Observable } from 'rxjs/Observable';

import _ from 'lodash';

import { Helper } from '../utils/helper';
import { Constants } from '../utils/constants';

@Injectable()
export class AuthGuard implements CanActivate {

    constructor(private router: Router, private helper: Helper) {
    }

    private getCurrentUser() {
        let user: Object = null;

        if (!_.isEmpty(localStorage.getItem('currentUser'))) {
            user = JSON.parse(localStorage.getItem('currentUser'));
        }

        return user;
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | Observable<boolean> | Promise<boolean> {
        const path = route.routeConfig.path;
        const user = this.getCurrentUser();

        let canActivate = false;
        let pathRedirect = '';

        if (_.isNil(user) && path !== Constants.LOGIN_PATH) {
            canActivate = false;
            pathRedirect = Constants.LOGIN_PATH;
        } else if (!_.isNil(user) && path === Constants.LOGIN_PATH) {
            canActivate = false;

            if (this.helper.isAdmin(user)) {
                pathRedirect = Constants.MANAGER_PATH;
            } else {
                pathRedirect = Constants.PROFILE_PATH;
            }
        } else {
            canActivate = true;
        }

        if (!canActivate && !_.isEmpty(pathRedirect)) {
            this.router.navigate([pathRedirect]);
        }
        return canActivate;
    }

}
