/*
Navicat MySQL Data Transfer

Source Server         : mysql
Source Server Version : 50715
Source Host           : localhost:3306
Source Database       : pilot_pj

Target Server Type    : MYSQL
Target Server Version : 50715
File Encoding         : 65001

Date: 2017-08-30 11:46:48
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for account
-- ----------------------------
DROP TABLE IF EXISTS `account`;
CREATE TABLE `account` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `details_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unq_username` (`username`),
  KEY `fk_user_details` (`details_id`) USING BTREE,
  CONSTRAINT `fk_user_details` FOREIGN KEY (`details_id`) REFERENCES `user_details` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of account
-- ----------------------------
INSERT INTO `account` VALUES ('1', 'admin', '$2a$10$eTgD8Cmb5Tj8XwwrAH2n5OVrlDi4.NqaDaqb0.uIrcLYChZHji2nS', '1', '1');
INSERT INTO `account` VALUES ('2', 'user2', '$2a$10$eTgD8Cmb5Tj8XwwrAH2n5OVrlDi4.NqaDaqb0.uIrcLYChZHji2nS', '1', '2');
INSERT INTO `account` VALUES ('3', 'user3', '$2a$10$eTgD8Cmb5Tj8XwwrAH2n5OVrlDi4.NqaDaqb0.uIrcLYChZHji2nS', '1', '3');
INSERT INTO `account` VALUES ('4', 'user4', '$2a$10$eTgD8Cmb5Tj8XwwrAH2n5OVrlDi4.NqaDaqb0.uIrcLYChZHji2nS', '1', '4');
INSERT INTO `account` VALUES ('5', 'user5', '$2a$10$eTgD8Cmb5Tj8XwwrAH2n5OVrlDi4.NqaDaqb0.uIrcLYChZHji2nS', '1', '5');
INSERT INTO `account` VALUES ('6', 'user6', '$2a$10$eTgD8Cmb5Tj8XwwrAH2n5OVrlDi4.NqaDaqb0.uIrcLYChZHji2nS', '1', '6');
INSERT INTO `account` VALUES ('7', 'user7', '$2a$10$eTgD8Cmb5Tj8XwwrAH2n5OVrlDi4.NqaDaqb0.uIrcLYChZHji2nS', '1', '7');
INSERT INTO `account` VALUES ('8', 'user8', '$2a$10$eTgD8Cmb5Tj8XwwrAH2n5OVrlDi4.NqaDaqb0.uIrcLYChZHji2nS', '1', '8');
INSERT INTO `account` VALUES ('9', 'user9', '$2a$10$eTgD8Cmb5Tj8XwwrAH2n5OVrlDi4.NqaDaqb0.uIrcLYChZHji2nS', '1', '9');
INSERT INTO `account` VALUES ('10', 'user10', '$2a$10$eTgD8Cmb5Tj8XwwrAH2n5OVrlDi4.NqaDaqb0.uIrcLYChZHji2nS', '1', '10');
INSERT INTO `account` VALUES ('11', 'user11', '$2a$10$eTgD8Cmb5Tj8XwwrAH2n5OVrlDi4.NqaDaqb0.uIrcLYChZHji2nS', '1', '11');
INSERT INTO `account` VALUES ('12', 'user12', '$2a$10$eTgD8Cmb5Tj8XwwrAH2n5OVrlDi4.NqaDaqb0.uIrcLYChZHji2nS', '1', '12');
INSERT INTO `account` VALUES ('13', 'user13', '$2a$10$eTgD8Cmb5Tj8XwwrAH2n5OVrlDi4.NqaDaqb0.uIrcLYChZHji2nS', '1', '13');

-- ----------------------------
-- Table structure for permision
-- ----------------------------
DROP TABLE IF EXISTS `permision`;
CREATE TABLE `permision` (
  `account_id` int(10) unsigned NOT NULL,
  `role_id` varchar(5) NOT NULL,
  PRIMARY KEY (`account_id`,`role_id`),
  KEY `fk_role_id` (`role_id`),
  CONSTRAINT `fk_account_id` FOREIGN KEY (`account_id`) REFERENCES `account` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_role_id` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of permision
-- ----------------------------
INSERT INTO `permision` VALUES ('1', 'ADMIN');
INSERT INTO `permision` VALUES ('1', 'MEM');
INSERT INTO `permision` VALUES ('2', 'MEM');
INSERT INTO `permision` VALUES ('3', 'MEM');
INSERT INTO `permision` VALUES ('4', 'MEM');
INSERT INTO `permision` VALUES ('5', 'MEM');
INSERT INTO `permision` VALUES ('6', 'MEM');
INSERT INTO `permision` VALUES ('7', 'MEM');
INSERT INTO `permision` VALUES ('8', 'MEM');
INSERT INTO `permision` VALUES ('9', 'MEM');
INSERT INTO `permision` VALUES ('10', 'MEM');
INSERT INTO `permision` VALUES ('11', 'MEM');
INSERT INTO `permision` VALUES ('12', 'MEM');
INSERT INTO `permision` VALUES ('13', 'MEM');
INSERT INTO `permision` VALUES ('1', 'SUPRT');

-- ----------------------------
-- Table structure for role
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `id` varchar(5) NOT NULL,
  `name` varchar(50) NOT NULL,
  `priority` tinyint(1) NOT NULL,
  `description` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of role
-- ----------------------------
INSERT INTO `role` VALUES ('ADMIN', 'Administrator', '1', null);
INSERT INTO `role` VALUES ('MEM', 'Member', '2', null);
INSERT INTO `role` VALUES ('SUPRT', 'Support', '3', null);

-- ----------------------------
-- Table structure for user_details
-- ----------------------------
DROP TABLE IF EXISTS `user_details`;
CREATE TABLE `user_details` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `firstname` varchar(200) NOT NULL,
  `lastname` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `birthday` date DEFAULT NULL,
  `gender` tinyint(1) NOT NULL,
  `phone` varchar(15) DEFAULT NULL,
  `address` varchar(500) DEFAULT NULL,
  `avatar` varchar(500) DEFAULT NULL,
  `cre_by` int(10) unsigned DEFAULT NULL,
  `cre_ts` datetime DEFAULT NULL,
  `update_by` int(10) unsigned DEFAULT NULL,
  `update_ts` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_cre_by` (`cre_by`),
  KEY `fk_update_by` (`update_by`),
  CONSTRAINT `fk_cre_by` FOREIGN KEY (`cre_by`) REFERENCES `account` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_update_by` FOREIGN KEY (`update_by`) REFERENCES `account` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user_details
-- ----------------------------
INSERT INTO `user_details` VALUES ('1', 'Elvia', 'J. Alvarez', 'ElviaJAlvarez@dayrep.com', '1964-05-04', '1', '423-942-8776', '3831 Hershell Hollow Road, Jasper, TN 37347', null, '1', '2017-08-30 00:00:00', '1', '2017-08-30 00:00:00');
INSERT INTO `user_details` VALUES ('2', 'James', 'E. Craig', 'JamesECraig@jourrapide.com', '1949-02-19', '0', '209-862-5201', '1253 Richards Avenue, Mewman, CA 95360', null, '1', '2017-08-30 00:00:00', '1', '2017-08-30 00:00:00');
INSERT INTO `user_details` VALUES ('3', 'Joseph', 'E. Shirley', 'JosephEShirley@armyspy.com', '1971-03-11', '0', '334-403-6531', '1240 Fleming Street, Montgomery, AL 36104', null, '1', '2017-08-30 00:00:00', '1', '2017-08-30 00:00:00');
INSERT INTO `user_details` VALUES ('4', 'Kimberly', 'S. Smalley', 'KimberlySSmalley@rhyta.com', '1969-10-24', '1', '678-859-1671', '707 Mount Olive Road, Gainesville, GA 30501', null, '1', '2017-08-30 00:00:00', '1', '2017-08-30 00:00:00');
INSERT INTO `user_details` VALUES ('5', 'David', 'H. Crosslin', 'DavidHCrosslin@armyspy.com', '1971-11-08', '0', '801-635-3177', '2317 Tori Lane, Salt Lake City, UT 84104', null, '1', '2017-08-30 00:00:00', '1', '2017-08-30 00:00:00');
INSERT INTO `user_details` VALUES ('6', 'Gayle', 'K. Matos', 'GayleKMatos@armyspy.com', '1998-05-21', '0', '503-320-1813', '2080 Gateway Road, Portland, OR 97230', null, '1', '2017-08-30 00:00:00', '1', '2017-08-30 00:00:00');
INSERT INTO `user_details` VALUES ('7', 'Melissa', 'R. Weis', 'MelissaRWeis@rhyta.com', '1992-02-11', '0', '515-375-3690', '4926 Hazelwood Avenue, Gilmore City, IA 50541', null, '1', '2017-08-30 00:00:00', '1', '2017-08-30 00:00:00');
INSERT INTO `user_details` VALUES ('8', 'Imelda', 'J. Leiva', 'ImeldaJLeiva@rhyta.com', '1994-03-12', '0', '216-297-9843', '4486 Glenwood Avenue, South Euclid, OH 44121', null, '1', '2017-08-30 00:00:00', '1', '2017-08-30 00:00:00');
INSERT INTO `user_details` VALUES ('9', 'Kelly', 'G. Crain', 'KellyGCrain@rhyta.com', '1995-02-14', '0', '262-223-2626', '336 Pearcy Avenue, Milwaukee, WI 53202', null, '1', '2017-08-30 00:00:00', '1', '2017-08-30 00:00:00');
INSERT INTO `user_details` VALUES ('10', 'Gary', 'H. Thomas', 'GaryHThomas@dayrep.com', '1992-08-29', '1', '530-871-2505', '1183 Byers Lane, Chico, CA 95926', null, '1', '2017-08-30 00:00:00', '1', '2017-08-30 00:00:00');
INSERT INTO `user_details` VALUES ('11', 'John', 'A. Brazier', 'JohnABrazier@jourrapide.com', '1992-12-12', '1', '830-968-0225', '1794 Parrish Avenue, San Antonio, TX 78217', null, '1', '2017-08-30 00:00:00', '1', '2017-08-30 00:00:00');
INSERT INTO `user_details` VALUES ('12', 'Clarence', 'J. Thomas', 'ClarenceJThomas@teleworm.us', '1990-01-20', '1', '714-855-9977', '255 Sunny Day Drive, Irvine, CA 92614', null, '1', '2017-08-30 00:00:00', '1', '2017-08-30 00:00:00');
INSERT INTO `user_details` VALUES ('13', 'Laura', 'A. Smith', 'LauraASmith@armyspy.com', '1998-06-01', '0', '864-361-3223', '3384 Deer Haven Drive, Piedmont, SC 29673', null, '1', '2017-08-30 00:00:00', '1', '2017-08-30 00:00:00');
