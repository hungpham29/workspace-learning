/*
Navicat MySQL Data Transfer

Source Server         : mysql
Source Server Version : 50715
Source Host           : localhost:3306
Source Database       : pilot_pj

Target Server Type    : MYSQL
Target Server Version : 50715
File Encoding         : 65001

Date: 2017-08-30 10:48:19
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for account
-- ----------------------------
DROP TABLE IF EXISTS `account`;
CREATE TABLE `account` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `details_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unq_username` (`username`),
  KEY `fk_user_details` (`details_id`) USING BTREE,
  CONSTRAINT `fk_user_details` FOREIGN KEY (`details_id`) REFERENCES `user_details` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for permision
-- ----------------------------
DROP TABLE IF EXISTS `permision`;
CREATE TABLE `permision` (
  `account_id` int(10) unsigned NOT NULL,
  `role_id` varchar(5) NOT NULL,
  PRIMARY KEY (`account_id`,`role_id`),
  KEY `fk_role_id` (`role_id`),
  CONSTRAINT `fk_account_id` FOREIGN KEY (`account_id`) REFERENCES `account` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_role_id` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for role
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `id` varchar(5) NOT NULL,
  `name` varchar(50) NOT NULL,
  `priority` tinyint(1) NOT NULL,
  `description` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for user_details
-- ----------------------------
DROP TABLE IF EXISTS `user_details`;
CREATE TABLE `user_details` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `firstname` varchar(200) NOT NULL,
  `lastname` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `birthday` date DEFAULT NULL,
  `gender` tinyint(1) NOT NULL,
  `phone` varchar(15) DEFAULT NULL,
  `address` varchar(500) DEFAULT NULL,
  `avatar` varchar(500) DEFAULT NULL,
  `cre_by` int(10) unsigned DEFAULT NULL,
  `cre_ts` datetime DEFAULT NULL,
  `update_by` int(10) unsigned DEFAULT NULL,
  `update_ts` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_cre_by` (`cre_by`),
  KEY `fk_update_by` (`update_by`),
  CONSTRAINT `fk_cre_by` FOREIGN KEY (`cre_by`) REFERENCES `account` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_update_by` FOREIGN KEY (`update_by`) REFERENCES `account` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
