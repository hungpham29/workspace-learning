/*
 * Id: Application.java, 2:13:55 PM
 *
 * Copyright (c) 2017
 * All rights reserved.
 *
 */
package com.pilot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.PropertySource;

@SpringBootApplication
@PropertySource("classpath:messages.properties")
public class Application extends SpringBootServletInitializer {

	/**
	 * Builder configure for application.
	 * 
	 * @author hungpham
	 */
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
		return builder.sources(Application.class);
	}

	/**
	 * Run application.
	 * 
	 * @author hungpham
	 */
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

}
