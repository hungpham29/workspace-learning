/*
 * Id: AccountService.java, 10:38:12 AM
 *
 * Copyright (c) 2017
 * All rights reserved.
 *
 */
package com.pilot.account;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.pilot.entity.Account;

/**
 * AccountService
 *
 */
public interface AccountService {

	/**
	 * Get account information by id.
	 * 
	 * @param id
	 *            (int) The ID of account
	 * @return (Account) Account information
	 * @author hungpham
	 */
	Account getAccountById(int id);

	/**
	 * Get multi account by ids.
	 * 
	 * @param ids
	 * @return
	 * @author hungpham
	 */
	List<Account> getMultiAccountById(List<Integer> ids);

	/**
	 * Get account information by username.
	 * 
	 * @param username
	 * @return
	 * @author hungpham
	 */
	Account getAccountByUsername(String username);

	/**
	 * Get account information by username (view object).
	 * 
	 * @param username
	 *            (String) The username of account
	 * @return (AccountVO) Account information
	 * @author hungpham
	 */
	AccountVO getAccountVOByUsername(String username);

	/**
	 * Get list account by status.
	 * 
	 * @param active
	 * @return
	 * @author hungpham
	 */
	List<Account> getListAccountByStatus(boolean active);

	/**
	 * Get list account.
	 * 
	 * @param pageable
	 *            (Pageable) Page information
	 * @return List account
	 * @author hungpham
	 */
	Page<AccountVO> getListAccountVOWithPaging(Pageable pageable);

	/**
	 * Create new account.
	 * 
	 * @param accountVO
	 *            (AccountVO) Account information to create
	 * @return (AccountVO) Account created
	 * @author hungpham
	 */
	AccountVO createAccount(AccountVO accountVO);

	/**
	 * Edit account.
	 * 
	 * @param accountVO
	 *            (AccountVO) Account information to edit
	 * @return (AccountVO) Account edited.
	 * @author hungpham
	 */
	AccountVO updateAccount(AccountVO accountVO);

	/**
	 * Delete account.
	 * 
	 * @param accountId
	 * @author hungpham
	 */
	void deleteAccount(int accountId);

	/**
	 * Delete accounts.
	 * 
	 * @param accounts
	 * @author hungpham
	 */
	void deleteMultiAccount(List<Account> accountIds);

	/**
	 * Search all account by criteria.
	 * 
	 * @param param
	 * @param pageable
	 * @return
	 * @author hungpham
	 */
	Page<AccountVO> searchByCriteria(Param param);

	/**
	 * Update status for account.
	 * 
	 * @param accountVO
	 * @return
	 * @author hungpham
	 */
	AccountVO updateStatus(AccountVO accountVO);
}
