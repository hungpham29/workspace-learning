/*
 * Id: AccountRespository.java, 10:44:52 AM
 *
 * Copyright (c) 2017
 * All rights reserved.
 *
 */
package com.pilot.account;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.pilot.entity.Account;
import com.pilot.enums.Gender;

/**
 * @author hungpham
 *
 */
public interface AccountRespository extends JpaRepository<Account, Integer> {

	/**
	 * Find account in database by ID's account.
	 * 
	 * @param id
	 *            (int) The ID of account for which you want to find in database
	 * @return (Account) Account information
	 * @author hungpham
	 */
	Account findById(int id);

	/**
	 * Find account in database by username's account.
	 * 
	 * @param username
	 *            (String) The username's account
	 * @return (Account) Account information
	 * @author hungpham
	 */
	Account findByUsername(String username);

	/**
	 * Find all account have same status.
	 * 
	 * @param active
	 *            (boolean) The status of account ACTIVE/INACTIVE
	 * @return List account
	 * @author hungpham
	 */
	List<Account> findByStatus(boolean active);

	/**
	 * Search with username, firstname, lastname, email.
	 * 
	 * @param username
	 * @param firstname
	 * @param lastname
	 * @param email
	 * @param pageable
	 * @return
	 * @author hungpham
	 */
	@Query(value = "select acc from Account acc where acc.username like %?1% and acc.userDetails.firstname like %?2% and acc.userDetails.lastname like %?3% and acc.userDetails.email like %?4%")
	Page<Account> findAccountLikeCriteria(String username, String firstname, String lastname, String email,
			Pageable pageable);

	/**
	 * Search with username, firstname, lastname, email, gender.
	 * 
	 * @param username
	 * @param firstname
	 * @param lastname
	 * @param email
	 * @param gender
	 * @param pageable
	 * @return
	 * @author hungpham
	 */
	@Query(value = "select acc from Account acc where acc.username like %?1% and acc.userDetails.firstname like %?2% and acc.userDetails.lastname like %?3% and acc.userDetails.email like %?4% and acc.userDetails.gender = ?5")
	Page<Account> findAccountLikeCriteria(String username, String firstname, String lastname, String email,
			Gender gender, Pageable pageable);

	/**
	 * Search with username, firstname, lastname, email, date from, date to.
	 * 
	 * @param username
	 * @param firstname
	 * @param lastname
	 * @param email
	 * @param from
	 * @param to
	 * @param pageable
	 * @return
	 * @author hungpham
	 */
	@Query(value = "select acc from Account acc where acc.username like %?1% and acc.userDetails.firstname like %?2% and acc.userDetails.lastname like %?3% and acc.userDetails.email like %?4% and acc.userDetails.creTs between ?5 and ?6")
	Page<Account> findAccountLikeCriteria(String username, String firstname, String lastname, String email, Date from,
			Date to, Pageable pageable);

	/**
	 * Search with username, firstname, lastname, email, status.
	 * 
	 * @param username
	 * @param firstname
	 * @param lastname
	 * @param email
	 * @param status
	 * @param pageable
	 * @return
	 * @author hungpham
	 */
	@Query(value = "select acc from Account acc where acc.username like %?1% and acc.userDetails.firstname like %?2% and acc.userDetails.lastname like %?3% and acc.userDetails.email like %?4% and acc.status = ?5")
	Page<Account> findAccountLikeCriteria(String username, String firstname, String lastname, String email,
			boolean status, Pageable pageable);

	/**
	 * Search with username, firstname, lastname, email, gender, date from, date
	 * to.
	 * 
	 * @param username
	 * @param firstname
	 * @param lastname
	 * @param email
	 * @param gender
	 * @param from
	 * @param to
	 * @param pageable
	 * @return
	 * @author hungpham
	 */
	@Query(value = "select acc from Account acc where acc.username like %?1% and acc.userDetails.firstname like %?2% and acc.userDetails.lastname like %?3% and acc.userDetails.email like %?4% and acc.userDetails.gender = ?5 and acc.userDetails.creTs between ?6 and ?7")
	Page<Account> findAccountLikeCriteria(String username, String firstname, String lastname, String email,
			Gender gender, Date from, Date to, Pageable pageable);

	/**
	 * Search with username, firstname, lastname, email, gender, status.
	 * 
	 * @param username
	 * @param firstname
	 * @param lastname
	 * @param email
	 * @param gender
	 * @param status
	 * @param pageable
	 * @return
	 * @author hungpham
	 */
	@Query(value = "select acc from Account acc where acc.username like %?1% and acc.userDetails.firstname like %?2% and acc.userDetails.lastname like %?3% and acc.userDetails.email like %?4% and acc.userDetails.gender = ?5 and acc.status = ?6")
	Page<Account> findAccountLikeCriteria(String username, String firstname, String lastname, String email,
			Gender gender, boolean status, Pageable pageable);

	/**
	 * Search with username, firstname, lastname, email, status, date from, date
	 * to.
	 * 
	 * @param username
	 * @param firstname
	 * @param lastname
	 * @param email
	 * @param status
	 * @param from
	 * @param to
	 * @param pageable
	 * @return
	 * @author hungpham
	 */
	@Query(value = "select acc from Account acc where acc.username like %?1% and acc.userDetails.firstname like %?2% and acc.userDetails.lastname like %?3% and acc.userDetails.email like %?4% and acc.status = ?5 and acc.userDetails.creTs between ?6 and ?7")
	Page<Account> findAccountLikeCriteria(String username, String firstname, String lastname, String email,
			boolean status, Date from, Date to, Pageable pageable);

	/**
	 * Search with username, firstname, lastname, email, status, date from, date
	 * to, gender.
	 * 
	 * @param username
	 * @param firstname
	 * @param lastname
	 * @param email
	 * @param status
	 * @param from
	 * @param to
	 * @param gender
	 * @param pageable
	 * @return
	 * @author hungpham
	 */
	@Query(value = "select acc from Account acc where acc.username like %?1% and acc.userDetails.firstname like %?2% and acc.userDetails.lastname like %?3% and acc.userDetails.email like %?4% and acc.status = ?5 and acc.userDetails.creTs between ?6 and ?7 and acc.userDetails.gender = ?8")
	Page<Account> findAccountLikeCriteria(String username, String firstname, String lastname, String email,
			boolean status, Date from, Date to, Gender gender, Pageable pageable);

}
