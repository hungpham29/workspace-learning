/*
 * Id: AccountVO.java, 5:18:40 PM
 *
 * Copyright (c) 2017
 * All rights reserved.
 *
 */
package com.pilot.account;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.pilot.enums.Gender;
import com.pilot.enums.RoleType;
import com.pilot.utils.CustomerDateAndTimeDeserialize;

/**
 * @author hungpham
 *
 */
public class AccountVO implements Serializable {

	/**
	 * serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;

	private static final String EMAIL_PATTERN = "^\\w+([\\.-]?\\w+)*@\\w+([\\.-]?\\w+)*(\\.\\w{2,3})+$";

	private int id;

	@NotEmpty(message = "{com.pilot.account.username.empty}")
	@Length(min = 5, max = 50, message = "{com.pilot.account.username.length}")
	private String username;

	@NotEmpty(message = "{com.pilot.account.password.empty}")
	@Length(min = 8, max = 50, message = "{com.pilot.account.password.length}")
	private String password;

	@NotNull(message = "{com.pilot.account.role.empty}")
	@Size(min = 1, message = "{com.pilot.account.role.empty}")
	private List<RoleType> roles;

	private boolean status;

	@NotEmpty(message = "{com.pilot.account.firstname.empty}")
	@Length(min = 1, max = 200, message = "{com.pilot.account.firstname.length}")
	private String firstname;

	@NotEmpty(message = "{com.pilot.account.lastname.empty}")
	@Length(min = 1, max = 200, message = "{com.pilot.account.lastname.length}")
	private String lastname;

	@NotEmpty(message = "{com.pilot.account.email.empty}")
	@Length(max = 200, message = "{com.pilot.account.email.length}")
	@Pattern(regexp = EMAIL_PATTERN, message = "{com.pilot.account.email.pattern}")
	private String email;

	@JsonDeserialize(using = CustomerDateAndTimeDeserialize.class)
	private Date birthday;

	@NotNull(message = "{com.pilot.account.gender.empty}")
	private Gender gender;

	@Length(max = 15, message = "{com.pilot.account.phone.length}")
	private String phone;

	@Length(max = 500, message = "{com.pilot.account.address.length}")
	private String address;

	private String avatar;

	private Date creTs;

	private Date lastUpdateTs;

	private String lastUpdateBy;

	private boolean changePassword;

	private boolean delete;

	private MultipartFile fileAvatar;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public List<RoleType> getRoles() {
		return roles;
	}

	public void setRoles(List<RoleType> roles) {
		this.roles = roles;
	}

	public boolean getStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public Date getCreTs() {
		return creTs;
	}

	public void setCreTs(Date creTs) {
		this.creTs = creTs;
	}

	public Date getLastUpdateTs() {
		return lastUpdateTs;
	}

	public void setLastUpdateTs(Date lastUpdateTs) {
		this.lastUpdateTs = lastUpdateTs;
	}

	public String getLastUpdateBy() {
		return lastUpdateBy;
	}

	public void setLastUpdateBy(String lastUpdateBy) {
		this.lastUpdateBy = lastUpdateBy;
	}

	public boolean isChangePassword() {
		return changePassword;
	}

	public void setChangePassword(boolean changePassword) {
		this.changePassword = changePassword;
	}

	public boolean isDelete() {
		return delete;
	}

	public void setDelete(boolean delete) {
		this.delete = delete;
	}

	public MultipartFile getFileAvatar() {
		return fileAvatar;
	}

	public void setFileAvatar(MultipartFile fileAvatar) {
		this.fileAvatar = fileAvatar;
	}

}
