/*
 * Id: AccountController.java, 9:24:24 AM
 *
 * Copyright (c) 2017
 * All rights reserved.
 *
 */
package com.pilot.account;

import java.util.List;

import javax.validation.Valid;

import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.pilot.entity.Account;
import com.pilot.exception.PilotException;
import com.pilot.security.SecurityUserDetails;
import com.pilot.utils.Helper;

/**
 * @author hungpham
 *
 */
@RestController
@RequestMapping(value = "/api/account/")
public class AccountController {

	private static final Logger LOG = Logger.getLogger(AccountController.class);

	private static final String DUPLICATE_USERNAME_MES = "This username is already exist.";

	private static final String ACCOUNT_NOT_EXIST_MES = "This account does'n exist.";

	private static final String DEFAULT_ADMIN_ACC_MES = "Can't inactive %s account";

	private static final String DEFAULT_ADMIN_DEL_MES = "Can't delete %s account";

	@Value("${admin.username.default}")
	private String defaultAdmin;

	@Autowired
	private AccountService accountService;

	@RequestMapping(value = "/current-user", method = RequestMethod.POST)
	public SecurityUserDetails getCurrentUser(HttpServletRequest request) {
		LOG.debug("Get user logined");

		return this.getLoginedUser();
	}

	/**
	 * Get all account. Only administrator can be call this method.
	 * 
	 * @return List account
	 * @author hungpham
	 */
	@PreAuthorize("hasRole('ADMIN')")
	@RequestMapping(value = "/all-user", method = RequestMethod.POST)
	public Page<AccountVO> getAllAccountInfo(@RequestBody Param param) {
		LOG.debug("Get all user");

		Page<AccountVO> result = null;

		if (!Helper.isEmpty(param)) {

			Pageable pageRequest = new PageRequest(param.getPage(), param.getSize());

			result = this.accountService.getListAccountVOWithPaging(pageRequest);
		}

		return result;
	}

	/**
	 * Get account details by username's account.
	 * 
	 * @param username
	 *            (String) The username's account for which you want receive
	 *            information
	 * @return (AccountVO) Account details
	 * @throws PilotException
	 * @author hungpham
	 */
	@PreAuthorize("hasRole('ADMIN') OR #username == authentication.principal.username")
	@RequestMapping(value = "/user", method = RequestMethod.POST)
	public AccountVO getAccountInfoByUsername(@RequestBody String username) throws PilotException {
		LOG.debug("Get user information");

		return this.accountService.getAccountVOByUsername(username);

	}

	/**
	 * Create new account. Only administrator can be call this method.
	 * 
	 * @param accountVO
	 *            (AccountVO) Account information to create
	 * @param errors
	 *            (BindingResult) A variable to store list error when
	 *            application validate fields of account
	 * @return (AccountVO) Account created.
	 * @throws PilotException
	 * @author hungpham
	 */
	@PreAuthorize("hasRole('ADMIN')")
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public AccountVO createAccount(@Valid @RequestBody AccountVO accountVO, BindingResult errors)
			throws PilotException {
		LOG.debug("Create new account: " + accountVO.getUsername());

		AccountVO accCreated;

		if (errors.hasErrors()) {
			throw new PilotException(PilotException.DATA_ERROR, errors);
		} else if (!Helper.isEmpty(this.accountService.getAccountByUsername(accountVO.getUsername()))) {
			throw new PilotException(PilotException.DUPLICATE_USERNAME, DUPLICATE_USERNAME_MES);
		} else {
			accCreated = this.accountService.createAccount(accountVO);
		}

		return accCreated;

	}

	/**
	 * Edit account information. Only administrator can be call this method.
	 * 
	 * @param accountVO
	 *            (AccountVO) The account information to edit
	 * @param errors
	 *            (BindingResult) A variable to store list error when
	 *            application validate fields of account
	 * @return (AccountVO) Account edited
	 * @throws PilotException
	 * @author hungpham
	 */
	@PreAuthorize("hasRole('ADMIN') OR #username == authentication.principal.username")
	@RequestMapping(value = "/edit", method = RequestMethod.POST)
	public AccountVO editAccount(@Valid @RequestBody AccountVO accountVO, BindingResult errors) throws PilotException {
		LOG.debug("Edit user: " + accountVO.getUsername());

		AccountVO accEdited;

		if (errors.hasErrors()) {
			throw new PilotException(PilotException.DATA_ERROR, errors);
		} else if (Helper.isEmpty(this.accountService.getAccountByUsername(accountVO.getUsername()))) {
			throw new PilotException(PilotException.ACCOUNT_NOT_EXIST, ACCOUNT_NOT_EXIST_MES);
		} else {
			accEdited = this.accountService.updateAccount(accountVO);
		}

		return accEdited;
	}

	/**
	 * Delete user by user ID.
	 * 
	 * @param id
	 *            (int) The ID of user which you want to delete.
	 * @throws PilotException
	 * @author hungp
	 */
	@PreAuthorize("hasRole('ADMIN')")
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public boolean deleteAccount(@RequestBody AccountVO accountVO) throws PilotException {
		LOG.debug("Delete user: " + accountVO.getUsername());

		boolean result;

		if (Helper.isEmpty(this.accountService.getAccountById(accountVO.getId()))) {
			throw new PilotException(PilotException.ACCOUNT_NOT_EXIST, ACCOUNT_NOT_EXIST_MES);
		} else if (this.defaultAdmin.equalsIgnoreCase(accountVO.getUsername())) {
			throw new PilotException(PilotException.DEFAULT_ADMIN_EXCEPTION,
					String.format(DEFAULT_ADMIN_DEL_MES, this.defaultAdmin));
		} else {
			this.accountService.deleteAccount(accountVO.getId());
			result = true;
		}

		return result;

	}

	/**
	 * Delete users.
	 * 
	 * @param accountIds
	 * @throws PilotException
	 * @author hungpham
	 */
	@PreAuthorize("hasRole('ADMIN')")
	@RequestMapping(value = "/delete-multi", method = RequestMethod.POST)
	public boolean deleteMultiAccount(@RequestBody List<Integer> accountIds) throws PilotException {
		LOG.debug("Delete multi user");

		boolean result;

		List<Account> lstAccountDelete = this.accountService.getMultiAccountById(accountIds);

		if (Helper.isEmpty(lstAccountDelete)) {
			throw new PilotException(PilotException.ACCOUNT_NOT_EXIST, ACCOUNT_NOT_EXIST_MES);
		} else if (this.checkExistDefaultAdmin(lstAccountDelete)) {
			throw new PilotException(PilotException.DEFAULT_ADMIN_EXCEPTION,
					String.format(DEFAULT_ADMIN_DEL_MES, this.defaultAdmin));
		} else {
			this.accountService.deleteMultiAccount(lstAccountDelete);
			result = true;
		}

		return result;
	}

	/**
	 * Check exist default admin account.
	 * 
	 * @param accounts
	 * @return
	 * @author hungpham
	 */
	private boolean checkExistDefaultAdmin(List<Account> accounts) {
		return accounts.stream().filter(acc -> this.defaultAdmin.equalsIgnoreCase(acc.getUsername())).count() == 1;
	}

	/**
	 * Search user by conditions.
	 * 
	 * @param param
	 * @return
	 * @throws PilotException
	 * @author hungpham
	 */
	@PreAuthorize("hasRole('ADMIN')")
	@RequestMapping(value = "/search-by-criteria", method = RequestMethod.POST)
	public Page<AccountVO> searchByCriteria(@RequestBody Param param) throws PilotException {
		LOG.debug("Search all user by criteria");

		return this.accountService.searchByCriteria(param);
	}

	/**
	 * Update status for account.
	 * 
	 * @param accountVO
	 * @return
	 * @throws PilotException
	 * @author hungpham
	 */
	@PreAuthorize("hasRole('ADMIN')")
	@RequestMapping(value = "/update-status", method = RequestMethod.POST)
	public AccountVO updateStatus(@RequestBody AccountVO accountVO) throws PilotException {
		LOG.debug("Update status of account: " + accountVO.getUsername());
		AccountVO result = null;

		if (!Helper.isEmpty(accountVO)) {
			if (this.defaultAdmin.equalsIgnoreCase(accountVO.getUsername())) {
				throw new PilotException(PilotException.DEFAULT_ADMIN_EXCEPTION,
						String.format(DEFAULT_ADMIN_ACC_MES, this.defaultAdmin));
			} else {
				result = this.accountService.updateStatus(accountVO);
			}
		}
		return result;

	}

	/**
	 * Get current user.
	 * 
	 * @author hungpham
	 */
	private SecurityUserDetails getLoginedUser() {
		return (SecurityUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	}
}
