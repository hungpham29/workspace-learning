/*
 * Id: AccountServiceImpl.java, 10:42:18 AM
 *
 * Copyright (c) 2017
 * All rights reserved.
 *
 */
package com.pilot.account;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.pilot.entity.Account;
import com.pilot.entity.Role;
import com.pilot.entity.UserDetails;
import com.pilot.enums.RoleType;
import com.pilot.security.SecurityUserDetails;
import com.pilot.utils.Constants;
import com.pilot.utils.Helper;

/**
 * @author hungpham
 *
 */
@Service
public class AccountServiceImpl implements AccountService {

	private static final Logger LOG = Logger.getLogger(AccountServiceImpl.class);

	@Autowired
	private AccountRespository accountRespository;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Override
	public Account getAccountById(int id) {
		return this.accountRespository.findById(id);
	}

	@Override
	public List<Account> getMultiAccountById(List<Integer> ids) {
		return this.accountRespository.findAll(ids);
	}

	@Override
	public Account getAccountByUsername(String username) {
		return this.accountRespository.findByUsername(username);
	}

	@Override
	public AccountVO getAccountVOByUsername(String username) {
		return this.convertAccountToViewObject(this.accountRespository.findByUsername(username), true);
	}

	@Override
	public List<Account> getListAccountByStatus(boolean active) {
		return this.accountRespository.findByStatus(active);
	}

	@Override
	public Page<AccountVO> getListAccountVOWithPaging(Pageable pageable) {

		Page<Account> page = this.accountRespository.findAll(pageable);

		return new PageImpl<>(page.getContent().stream().map(acc -> this.convertAccountToViewObject(acc, true))
				.collect(Collectors.toList()), pageable, page.getTotalElements());

	}

	@Override
	@Transactional()
	public AccountVO createAccount(AccountVO accountVO) {
		AccountVO accCreated = null;
		Account acc = this.prepareAccountToCreate(accountVO);
		try {
			if (!Helper.isEmpty(acc)) {
				// Create new account.
				this.accountRespository.save(acc);

				// Get account created.
				accCreated = this.convertAccountToViewObject(this.accountRespository.findByUsername(acc.getUsername()),
						true);
			}
		} catch (DataAccessException e) {
			LOG.error(e.getMessage(), e);
		}

		return accCreated;
	}

	/**
	 * Convert AccountVO object to Account object.
	 * 
	 * @param accountVO
	 *            (AccountVO) Account to convert
	 * @return (Account) Account object
	 * @author hungpham
	 */
	private Account prepareAccountToCreate(AccountVO accountVO) {
		Account account = null;

		if (!Helper.isEmpty(accountVO)) {

			Date currentDate = new Date();
			int currentUserId = this.getLoginedUser().getUserId();

			UserDetails userDetails = new UserDetails();
			userDetails.setFirstname(accountVO.getFirstname());
			userDetails.setLastname(accountVO.getLastname());
			userDetails.setEmail(accountVO.getEmail());
			userDetails.setBirthday(accountVO.getBirthday());
			userDetails.setGender(accountVO.getGender());
			userDetails.setPhone(accountVO.getPhone());
			userDetails.setAddress(accountVO.getAddress());
			userDetails.setCreBy(currentUserId);
			userDetails.setCreTs(currentDate);
			userDetails.setUpdateBy(currentUserId);
			userDetails.setUpdateTs(currentDate);

			List<Role> roles = new ArrayList<>();
			accountVO.getRoles().forEach(roleTyp -> {
				Role role = new Role();
				role.setId(roleTyp);
				roles.add(role);
			});

			account = new Account();
			account.setUsername(accountVO.getUsername());
			account.setStatus(true);
			account.setPassword(this.passwordEncoder.encode(accountVO.getPassword()));
			account.setUserDetails(userDetails);
			account.setRoles(roles);

		}

		return account;
	}

	/**
	 * Convert Account object to view object (without password).
	 * 
	 * @param acc
	 *            (Account) Account object to convert
	 * @param withoutPass
	 *            (boolean) If value of parameter is true, password will be
	 *            ignore
	 * @return Account view object
	 * @author hungpham
	 */
	private AccountVO convertAccountToViewObject(Account acc, boolean withoutPass) {
		AccountVO vo = null;
		if (!Helper.isEmpty(acc)) {
			vo = new AccountVO();

			vo.setId(acc.getId());
			vo.setUsername(acc.getUsername());

			if (!withoutPass) {
				vo.setPassword(acc.getPassword());
			}

			List<RoleType> roles = acc.getRoles().stream().map(Role::getId).collect(Collectors.toList());
			vo.setRoles(roles);

			vo.setStatus(acc.isStatus());
			vo.setFirstname(acc.getUserDetails().getFirstname());
			vo.setLastname(acc.getUserDetails().getLastname());
			vo.setEmail(acc.getUserDetails().getEmail());
			vo.setBirthday(acc.getUserDetails().getBirthday());
			vo.setGender(acc.getUserDetails().getGender());
			vo.setPhone(acc.getUserDetails().getPhone());
			vo.setAddress(acc.getUserDetails().getAddress());
			vo.setAvatar(acc.getUserDetails().getAvatar());
			vo.setCreTs(acc.getUserDetails().getCreTs());
			vo.setLastUpdateTs(acc.getUserDetails().getUpdateTs());
			vo.setLastUpdateBy(acc.getUserDetails().getUpdateByUser().getFullName());
		}

		return vo;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public AccountVO updateAccount(AccountVO accountVO) {
		AccountVO accEdited = null;

		if (!Helper.isEmpty(accountVO)) {
			Account acc = this.accountRespository.findById(accountVO.getId());

			this.prepareAccountToEdit(acc, accountVO);

			if (!Helper.isEmpty(acc)) {
				this.accountRespository.save(acc);
			}

			// Get account created.
			accEdited = this.convertAccountToViewObject(this.accountRespository.findByUsername(acc.getUsername()),
					true);
		}

		return accEdited;
	}

	private void prepareAccountToEdit(Account account, AccountVO accountVO) {
		if (!Helper.isEmpty(account)) {

			Date currentDate = new Date();
			int currentUserId = this.getLoginedUser().getUserId();

			UserDetails userDetails = account.getUserDetails();

			if (!Helper.isEmpty(userDetails)) {
				userDetails.setFirstname(accountVO.getFirstname());
				userDetails.setLastname(accountVO.getLastname());
				userDetails.setEmail(accountVO.getEmail());
				userDetails.setBirthday(accountVO.getBirthday());
				userDetails.setGender(accountVO.getGender());
				userDetails.setPhone(accountVO.getPhone());
				userDetails.setAddress(accountVO.getAddress());
				userDetails.setUpdateBy(currentUserId);
				userDetails.setUpdateTs(currentDate);
			}

			// Update role for user.
			List<Role> roles = account.getRoles();
			List<Role> rolesNew = new ArrayList<>();

			if (!Helper.isEmpty(accountVO.getRoles())) {
				accountVO.getRoles().forEach(roleTyp -> {
					Role role = new Role(roleTyp);
					rolesNew.add(role);
				});

				while (roles.size() > 0) {
					roles.remove(0);
				}

				rolesNew.forEach(role -> roles.add(role));
			}

			account.setPassword(this.passwordEncoder.encode(accountVO.getPassword()));
		}
	}

	/**
	 * Get current user.
	 * 
	 * @return Current user
	 * @author hungpham
	 */
	private SecurityUserDetails getLoginedUser() {
		return (SecurityUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	}

	@Override
	public void deleteAccount(int accountID) {
		this.accountRespository.delete(accountID);
	}

	@Override
	public void deleteMultiAccount(List<Account> accounts) {
		this.accountRespository.delete(accounts);
	}

	@Override
	public Page<AccountVO> searchByCriteria(Param param) {
		Page<Account> page;

		Pageable pageable = new PageRequest(param.getPage(), param.getSize());

		if (!Helper.isEmpty(param.getStatus())) {
			page = this.searchWithStatus(param);
		} else {
			page = this.searchWithoutStatus(param);
		}

		return new PageImpl<>(page.getContent().stream().map(acc -> this.convertAccountToViewObject(acc, true))
				.collect(Collectors.toList()), pageable, page.getTotalElements());
	}

	/**
	 * Search without status.
	 * 
	 * @param param
	 * @return
	 * @author hungpham
	 */
	private Page<Account> searchWithoutStatus(Param param) {
		Page<Account> page;

		Pageable pageable = new PageRequest(param.getPage(), param.getSize());
		String username = !Helper.isEmpty(param.getUsername()) ? param.getUsername() : Constants.EMPTY_STRING;
		String firstname = !Helper.isEmpty(param.getFirstname()) ? param.getFirstname() : Constants.EMPTY_STRING;
		String lastname = !Helper.isEmpty(param.getLastname()) ? param.getLastname() : Constants.EMPTY_STRING;
		String email = !Helper.isEmpty(param.getEmail()) ? param.getEmail() : Constants.EMPTY_STRING;

		if (!Helper.isEmpty(param.getGender()) && Helper.isEmpty(param.getCreateDateFrom(), param.getCreateDateTo())) {
			page = this.accountRespository.findAccountLikeCriteria(username, firstname, lastname, email,
					param.getGender(), pageable);
		} else if (Helper.isEmpty(param.getGender())
				&& !Helper.isEmpty(param.getCreateDateFrom(), param.getCreateDateTo())) {
			page = this.accountRespository.findAccountLikeCriteria(username, firstname, lastname, email,
					param.getCreateDateFrom(), param.getCreateDateTo(), pageable);
		} else if (!Helper.isEmpty(param.getGender())
				&& !Helper.isEmpty(param.getCreateDateFrom(), param.getCreateDateTo())) {
			page = this.accountRespository.findAccountLikeCriteria(username, firstname, lastname, email,
					param.getGender(), param.getCreateDateFrom(), param.getCreateDateTo(), pageable);
		} else if (Helper.oneParamNotEmpty(username, firstname, lastname, email)) {
			page = this.accountRespository.findAccountLikeCriteria(username, firstname, lastname, email, pageable);
		} else {
			page = this.accountRespository.findAll(pageable);
		}

		return page;
	}

	/**
	 * Search with status.
	 * 
	 * @param param
	 * @return
	 * @author hungpham
	 */
	private Page<Account> searchWithStatus(Param param) {
		Page<Account> page;

		Pageable pageable = new PageRequest(param.getPage(), param.getSize());
		String username = !Helper.isEmpty(param.getUsername()) ? param.getUsername() : Constants.EMPTY_STRING;
		String firstname = !Helper.isEmpty(param.getFirstname()) ? param.getFirstname() : Constants.EMPTY_STRING;
		String lastname = !Helper.isEmpty(param.getLastname()) ? param.getLastname() : Constants.EMPTY_STRING;
		String email = !Helper.isEmpty(param.getEmail()) ? param.getEmail() : Constants.EMPTY_STRING;

		if (!Helper.isEmpty(param.getGender()) && Helper.isEmpty(param.getCreateDateFrom(), param.getCreateDateTo())) {
			page = this.accountRespository.findAccountLikeCriteria(username, firstname, lastname, email,
					param.getGender(), param.getStatus(), pageable);
		} else if (Helper.isEmpty(param.getGender())
				&& !Helper.isEmpty(param.getCreateDateFrom(), param.getCreateDateTo())) {
			page = this.accountRespository.findAccountLikeCriteria(username, firstname, lastname, email,
					param.getStatus(), param.getCreateDateFrom(), param.getCreateDateTo(), pageable);
		} else if (!Helper.isEmpty(param.getGender())
				&& !Helper.isEmpty(param.getCreateDateFrom(), param.getCreateDateTo())) {
			page = this.accountRespository.findAccountLikeCriteria(username, firstname, lastname, email,
					param.getStatus(), param.getCreateDateFrom(), param.getCreateDateTo(), param.getGender(), pageable);
		} else {
			page = this.accountRespository.findAccountLikeCriteria(username, firstname, lastname, email,
					param.getStatus(), pageable);
		}

		return page;
	}

	@Override
	public AccountVO updateStatus(AccountVO accountVO) {
		AccountVO accEdited = null;
		Account acc = this.accountRespository.findByUsername(accountVO.getUsername());

		if (!Helper.isEmpty(acc)) {
			acc.setStatus(accountVO.getStatus());
			this.accountRespository.save(acc);

			accEdited = this.convertAccountToViewObject(acc, true);
		}

		return accEdited;
	}

	/**
	 * Check account has role ADMIN.
	 * 
	 * @param acc
	 * @return
	 * @author hungpham
	 */
	private boolean hasRoleAdmin(Account acc) {

		return acc.getRoles().stream().filter(r -> r.getId().equals(RoleType.ADMIN)).count() == 1;

	}

}
