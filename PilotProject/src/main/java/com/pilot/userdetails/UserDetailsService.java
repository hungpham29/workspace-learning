/*
 * Id: UserDetailsService.java, 11:01:28 AM
 *
 * Copyright (c) 2017
 * All rights reserved.
 *
 */
package com.pilot.userdetails;

import com.pilot.entity.Account;
import com.pilot.entity.UserDetails;

/**
 * @author hungpham
 *
 */
public interface UserDetailsService {

	/**
	 * Get account information by id.
	 * 
	 * @author hungpham
	 */
	Account getAccountById(int id);

	/**
	 * Get account information by username.
	 * 
	 * @author hungpham
	 */
	Account getAccountByUsername(String username);

	/**
	 * Get only user details by id.
	 * 
	 * @author hungpham
	 */
	UserDetailsVO getUserDetailsByUsername(String username);

	/**
	 * Update user detauls for user.
	 * 
	 * @param details
	 * @return
	 */
	UserDetailsVO updateUserDetails(UserDetailsVO detailsVO);
}
