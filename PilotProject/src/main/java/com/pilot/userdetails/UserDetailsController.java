/*
 * Id: UserDetailsController.java, 2:13:55 PM
 *
 * Copyright (c) 2017
 * All rights reserved.
 *
 */
package com.pilot.userdetails;

import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.pilot.entity.UserDetails;
import com.pilot.exception.PilotException;

@RestController
@RequestMapping(value = "/api/user-details")
public class UserDetailsController {
	private static final Logger LOG = Logger.getLogger(UserDetailsController.class);

	@Autowired
	UserDetailsService userDetailsService;

	@RequestMapping(value = "/user", method = RequestMethod.POST)
	public UserDetailsVO getUserDetails(@RequestBody String username) {
		LOG.info(username);
		return this.userDetailsService.getUserDetailsByUsername(username);
	}

	@RequestMapping(value = "/update-user-details", method = RequestMethod.POST)
	public UserDetailsVO updateUserDetails(@Valid @RequestBody UserDetailsVO detailsVO, BindingResult errors)
			throws PilotException {

		UserDetailsVO detailsEdited = null;

		if (errors.hasErrors()) {
			throw new PilotException(PilotException.DATA_ERROR, errors);
		} else {
			detailsEdited = this.userDetailsService.updateUserDetails(detailsVO);
		}

		return detailsEdited;
	}

}
