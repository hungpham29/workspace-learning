package com.pilot.userdetails;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.pilot.enums.Gender;
import com.pilot.utils.CustomerDateAndTimeDeserialize;

public class UserDetailsVO implements Serializable {

	/**
	 * serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;

	private static final String EMAIL_PATTERN = "^\\w+([\\.-]?\\w+)*@\\w+([\\.-]?\\w+)*(\\.\\w{2,3})+$";

	private int id;

	@NotEmpty(message = "{com.pilot.account.firstname.empty}")
	@Length(min = 1, max = 200, message = "{com.pilot.account.firstname.length}")
	private String firstname;

	@NotEmpty(message = "{com.pilot.account.lastname.empty}")
	@Length(min = 1, max = 200, message = "{com.pilot.account.lastname.length}")
	private String lastname;

	@NotEmpty(message = "{com.pilot.account.email.empty}")
	@Length(max = 200, message = "{com.pilot.account.email.length}")
	@Pattern(regexp = EMAIL_PATTERN, message = "{com.pilot.account.email.pattern}")
	private String email;

	@JsonDeserialize(using = CustomerDateAndTimeDeserialize.class)
	private Date birthday;

	@NotNull(message = "{com.pilot.account.gender.empty}")
	private Gender gender;

	@Length(max = 15, message = "{com.pilot.account.phone.length}")
	private String phone;

	@Length(max = 500, message = "{com.pilot.account.address.length}")
	private String address;

	private String avatar;

	private Date creTs;

	private Date lastUpdateTs;

	private String lastUpdateBy;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public Date getCreTs() {
		return creTs;
	}

	public void setCreTs(Date creTs) {
		this.creTs = creTs;
	}

	public Date getLastUpdateTs() {
		return lastUpdateTs;
	}

	public void setLastUpdateTs(Date lastUpdateTs) {
		this.lastUpdateTs = lastUpdateTs;
	}

	public String getLastUpdateBy() {
		return lastUpdateBy;
	}

	public void setLastUpdateBy(String lastUpdateBy) {
		this.lastUpdateBy = lastUpdateBy;
	}

}
