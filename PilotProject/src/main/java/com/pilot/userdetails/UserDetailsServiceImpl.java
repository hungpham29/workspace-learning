/*
 * Id: UserDetailsServiceImpl.java, 11:04:31 AM
 *
 * Copyright (c) 2017
 * All rights reserved.
 *
 */
package com.pilot.userdetails;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.pilot.account.AccountRespository;
import com.pilot.account.AccountVO;
import com.pilot.entity.Account;
import com.pilot.entity.Role;
import com.pilot.entity.UserDetails;
import com.pilot.enums.RoleType;
import com.pilot.security.SecurityUserDetails;
import com.pilot.utils.Helper;

/**
 * @author hungpham
 *
 */
@Service
public class UserDetailsServiceImpl implements UserDetailsService {

	private static final Logger LOG = Logger.getLogger(UserDetailsServiceImpl.class);

	@Autowired
	private UserDetailsRepository userDetailsRespository;

	@Autowired
	private AccountRespository accountRespository;

	@Override
	public Account getAccountById(int id) {
		return this.accountRespository.findById(id);
	}

	@Override
	public Account getAccountByUsername(String username) {
		return this.accountRespository.findByUsername(username);
	}

	@Override
	public UserDetailsVO getUserDetailsByUsername(String username) {
		return this.convertUserDetailsToViewObject(this.userDetailsRespository.findByUsername(username));
	}

	private UserDetailsVO convertUserDetailsToViewObject(UserDetails details) {
		UserDetailsVO vo = null;
		if (!Helper.isEmpty(details)) {
			vo = new UserDetailsVO();

			vo.setId(details.getId());

			vo.setFirstname(details.getFirstname());
			vo.setLastname(details.getLastname());
			vo.setEmail(details.getEmail());
			vo.setBirthday(details.getBirthday());
			vo.setGender(details.getGender());
			vo.setPhone(details.getPhone());
			vo.setAddress(details.getAddress());
			vo.setAvatar(details.getAvatar());
			vo.setCreTs(details.getCreTs());
			vo.setLastUpdateTs(details.getUpdateTs());
			vo.setLastUpdateBy(details.getUpdateByUser().getFullName());
		}

		return vo;
	}

	@Override
	public UserDetailsVO updateUserDetails(UserDetailsVO detailsVO) {
		UserDetailsVO detailEdited = null;

		if (!Helper.isEmpty(detailsVO)) {
			UserDetails details = this.userDetailsRespository.findById(detailsVO.getId());

			this.prepareUserDetailsToEdit(details, detailsVO);

			if (!Helper.isEmpty(details)) {
				this.userDetailsRespository.save(details);
			}

			// Get user details updated.
			detailEdited = this.convertUserDetailsToViewObject(this.userDetailsRespository.findById(detailsVO.getId()));
		}

		return detailEdited;
	}

	private void prepareUserDetailsToEdit(UserDetails details, UserDetailsVO detailsVO) {

		Date currentDate = new Date();
		int currentUserId = this.getLoginedUser().getUserId();

		if (!Helper.isEmpty(details)) {
			details.setFirstname(detailsVO.getFirstname());
			details.setLastname(detailsVO.getLastname());
			details.setEmail(detailsVO.getEmail());
			details.setBirthday(detailsVO.getBirthday());
			details.setGender(detailsVO.getGender());
			details.setPhone(detailsVO.getPhone());
			details.setAddress(detailsVO.getAddress());
			details.setUpdateBy(currentUserId);
			details.setUpdateTs(currentDate);
		}
	}

	/**
	 * Get current user.
	 * 
	 * @return Current user
	 * @author hungpham
	 */
	private SecurityUserDetails getLoginedUser() {
		return (SecurityUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	}

}
