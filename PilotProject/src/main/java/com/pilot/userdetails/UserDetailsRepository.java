/*
 * Id: UserDetailsRepository.java, 2:13:55 PM
 *
 * Copyright (c) 2017
 * All rights reserved.
 *
 */
package com.pilot.userdetails;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.pilot.entity.UserDetails;

public interface UserDetailsRepository extends CrudRepository<UserDetails, Integer> {

	/**
	 * Get user details in database by id.
	 * 
	 * @param id
	 *            (int) The ID of user details.
	 * @return UserDetails object
	 * @author hungpham
	 */
	UserDetails findById(int id);

	/**
	 * Find user details in database by username.
	 * 
	 * @param username
	 * @return
	 */
	@Query(value = "select userDetails from UserDetails userDetails where userDetails.account.username = ?1")
	UserDetails findByUsername(String username);
}
