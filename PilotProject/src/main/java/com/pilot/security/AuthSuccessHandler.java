/*
 * Id: AuthSuccessHandler.java, 2:17:28 PM
 *
 * Copyright (c) 2017
 * All rights reserved.
 *
 */
package com.pilot.security;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author hungpham
 *
 */
@Component
public class AuthSuccessHandler extends SavedRequestAwareAuthenticationSuccessHandler {
	private static final Logger LOG = Logger.getLogger(AuthSuccessHandler.class);

	private final ObjectMapper mapper;

	@Autowired
	AuthSuccessHandler(MappingJackson2HttpMessageConverter messageConverter) {
		this.mapper = messageConverter.getObjectMapper();
	}

	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
			Authentication authentication) throws ServletException, IOException {
		response.setStatus(HttpServletResponse.SC_OK);

		SecurityUserDetails userDetails = (SecurityUserDetails) authentication.getPrincipal();

		LOG.info(userDetails.getUsername() + " had logined.");

		PrintWriter writer = response.getWriter();
		mapper.writeValue(writer, userDetails);
		writer.flush();
	}

}
