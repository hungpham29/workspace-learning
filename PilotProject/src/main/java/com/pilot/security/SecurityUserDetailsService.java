/*
 * Id: SecurityUserDetailsService.java, 2:33:48 PM
 *
 * Copyright (c) 2017
 * All rights reserved.
 *
 */
package com.pilot.security;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import com.pilot.account.AccountService;
import com.pilot.entity.Account;
import com.pilot.entity.Role;
import com.pilot.enums.Gender;
import com.pilot.utils.Constants;
import com.pilot.utils.Helper;

/**
 * @author hungpham
 *
 */
@Component
public class SecurityUserDetailsService implements UserDetailsService {

	private static final Logger LOG = Logger.getLogger(SecurityUserDetailsService.class);

	@Autowired
	private AccountService accountService;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Account account = this.accountService.getAccountByUsername(username);
		if (account == null) {
			throw new UsernameNotFoundException("User not found");
		}
		return this.buildUserDetails(account);
	}

	/**
	 * Build user details for Security.
	 * 
	 * @param acc
	 *            (Account) Account information
	 * @return Security user details object
	 * @author hungpham
	 */
	private SecurityUserDetails buildUserDetails(Account acc) {
		String username = acc.getUsername();
		String password = acc.getPassword();
		boolean enabled = acc.isStatus();
		boolean accountNonExpired = true;
		boolean credentialsNonExpired = true;
		boolean accountNonLocked = true;

		List<GrantedAuthority> authorities = new ArrayList<>();
		if (!Helper.isEmpty(acc.getRoles())) {
			acc.getRoles()
					.forEach(role -> authorities.add(new SimpleGrantedAuthority(Constants.PREFIX_ROLE + role.getId())));
		}

		// More information.
		String firstname = acc.getUserDetails().getFirstname();
		String lastname = acc.getUserDetails().getLastname();
		int userId = acc.getId();
		Gender gender = acc.getUserDetails().getGender();

		return new SecurityUserDetails(username, password, enabled, accountNonExpired, credentialsNonExpired,
				accountNonLocked, authorities, userId, firstname, lastname, gender);

	}

}
