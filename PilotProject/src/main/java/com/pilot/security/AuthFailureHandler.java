/*
 * Id: AuthFailureHandler.java, 2:30:36 PM
 *
 * Copyright (c) 2017
 * All rights reserved.
 *
 */
package com.pilot.security;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pilot.exception.ErrorResponse;

/**
 * @author hungpham
 *
 */
@Component
public class AuthFailureHandler extends SimpleUrlAuthenticationFailureHandler {

	private static final Logger LOG = Logger.getLogger(AuthFailureHandler.class);

	private final ObjectMapper mapper;

	@Autowired
	public AuthFailureHandler(MappingJackson2HttpMessageConverter messageConverter) {
		this.mapper = messageConverter.getObjectMapper();
	}

	@Override
	public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException exception) throws IOException, ServletException {

		LOG.error(exception.getMessage(), exception);

		ErrorResponse error = new ErrorResponse();
		error.setError(exception.getClass().getSimpleName());
		error.setStatus(HttpStatus.UNAUTHORIZED.value());
		error.setMessage(exception.getMessage());

		// response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);

		PrintWriter writer = response.getWriter();
		// writer.write(exception.getMessage());
		this.mapper.writeValue(writer, error);
		writer.flush();
	}

}
