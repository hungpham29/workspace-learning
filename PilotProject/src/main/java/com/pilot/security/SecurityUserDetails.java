/*
 * Id: SecurityUserDetails.java, 2:21:49 PM
 *
 * Copyright (c) 2017
 * All rights reserved.
 *
 */
package com.pilot.security;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import com.pilot.enums.Gender;

/**
 * @author hungpham
 *
 */
public class SecurityUserDetails extends User {
	/**
	 * serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;

	private int userId;
	private String firstname;
	private String lastname;
	private Gender gender;

	public SecurityUserDetails(String username, String password, Collection<? extends GrantedAuthority> authorities,
			int userId, String firstname, String lastname, Gender gender) {
		super(username, password, true, true, true, true, authorities);
		this.userId = userId;
		this.firstname = firstname;
		this.lastname = lastname;
		this.gender = gender;
	}

	public SecurityUserDetails(String username, String password, boolean enabled, boolean accountNonExpired,
			boolean credentialsNonExpired, boolean accountNonLocked, Collection<? extends GrantedAuthority> authorities,
			int userId, String firstname, String lastname, Gender gender) {
		super(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
		this.userId = userId;
		this.firstname = firstname;
		this.lastname = lastname;
		this.gender = gender;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

}
