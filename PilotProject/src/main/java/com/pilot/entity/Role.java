/*
 * Id: Role.java, 4:40:50 PM
 *
 * Copyright (c) 2017
 * All rights reserved.
 *
 */
package com.pilot.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;

import com.pilot.enums.RoleType;
import com.pilot.utils.Helper;

/**
 * @author hungpham
 *
 */
@Entity
@Table(name = "role")
public class Role implements Serializable {

	/**
	 * serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id", unique = true, nullable = false)
	@Enumerated(value = EnumType.STRING)
	private RoleType id;

	@Column(name = "name", unique = true, nullable = false)
	private String name;

	@Column(name = "description")
	private String description;

	@Column(name = "priority", nullable = false)
	private int priority;

	/**
	 * Constructor.
	 */
	public Role() {
		// Default constructor.
	}

	/**
	 * Constructor with id parameter.
	 * 
	 * @param id
	 */
	public Role(RoleType id) {
		this.id = id;
	}

	public RoleType getId() {
		return id;
	}

	public void setId(RoleType id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	@Override
	public boolean equals(Object obj) {

		return obj instanceof Role && !Helper.isEmpty(((Role) obj).getId())
				&& ((Role) obj).getId().equals(this.getId());
	}
}
