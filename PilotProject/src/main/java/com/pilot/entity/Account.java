/*
 * Id: Account.java, 2:13:55 PM
 *
 * Copyright (c) 2017
 * All rights reserved.
 *
 */
package com.pilot.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.pilot.utils.Constants;
import com.pilot.utils.Helper;

/**
 * Account object.
 * 
 * @author hungpham
 *
 */
@Entity
@Table(name = "account", uniqueConstraints = { @UniqueConstraint(columnNames = "username") })
public class Account implements Serializable {

	/**
	 * serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private int id;

	@Column(name = "username", unique = true, nullable = false, length = 100)
	private String username;

	@JsonIgnore
	@Column(name = "password", nullable = false, length = 100)
	private String password;

	@Column(name = "status")
	private boolean status;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "details_id")
	private UserDetails userDetails;

	@ManyToMany(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
	@JoinTable(name = "permision", joinColumns = {
			@JoinColumn(name = "account_id", referencedColumnName = "id") }, inverseJoinColumns = {
					@JoinColumn(name = "role_id", referencedColumnName = "id") })
	private List<Role> roles;

	public Account() {
		// Default constructor.
	}

	public Account(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public UserDetails getUserDetails() {
		return userDetails;
	}

	public void setUserDetails(UserDetails userDetails) {
		this.userDetails = userDetails;
	}

	public List<Role> getRoles() {
		return roles;
	}

	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}

	public String getFullName() {
		String fullName = Constants.EMPTY_STRING;

		if (!Helper.isEmpty(this.getUserDetails())) {
			fullName = String.join(Constants.SPACE_STRING, this.getUserDetails().getFirstname(),
					this.getUserDetails().getLastname());
		}
		return fullName;
	}

}
