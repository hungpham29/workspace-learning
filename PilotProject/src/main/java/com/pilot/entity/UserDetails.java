/*
 * Id: UserDetails.java, 2:13:55 PM
 *
 * Copyright (c) 2017
 * All rights reserved.
 *
 */
package com.pilot.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.pilot.enums.Gender;
import com.pilot.utils.Constants;
import com.pilot.utils.Helper;

@Entity
@Table(name = "user_details")
public class UserDetails implements Serializable {

	/**
	 * serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id", unique = true, nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(name = "firstname", nullable = false, length = 200)
	private String firstname;

	@Column(name = "lastname", nullable = false, length = 200)
	private String lastname;

	@Column(name = "email", nullable = false, length = 200)
	private String email;

	@Column(name = "birthday")
	private Date birthday;

	@Column(name = "gender", nullable = false)
	@Enumerated(EnumType.ORDINAL)
	private Gender gender;

	@Column(name = "phone", length = 15)
	private String phone;

	@Column(name = "address", length = 500)
	private String address;

	@Column(name = "avatar", length = 500)
	private String avatar;

	@Column(name = "cre_by")
	private Integer creBy;

	@Column(name = "cre_ts")
	private Date creTs;

	@Column(name = "update_by")
	private Integer updateBy;

	@Column(name = "update_ts")
	private Date updateTs;

	@OneToOne(mappedBy = "userDetails")
	private Account account;

	@OneToOne
	@JoinColumn(name = "update_by", insertable = false, updatable = false)
	private Account updateByUser;

	public UserDetails() {
		// Default constructor.
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public Integer getCreBy() {
		return creBy;
	}

	public void setCreBy(Integer creBy) {
		this.creBy = creBy;
	}

	public Date getCreTs() {
		return creTs;
	}

	public void setCreTs(Date creTs) {
		this.creTs = creTs;
	}

	public Integer getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(Integer updateBy) {
		this.updateBy = updateBy;
	}

	public Date getUpdateTs() {
		return updateTs;
	}

	public void setUpdateTs(Date updateTs) {
		this.updateTs = updateTs;
	}

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	public Account getUpdateByUser() {
		return updateByUser;
	}

	public void setUpdateByUser(Account updateByUser) {
		this.updateByUser = updateByUser;
	}

	public String getFullName() {
		return String.join(Constants.SPACE_STRING, this.firstname, this.lastname);
	}
}
