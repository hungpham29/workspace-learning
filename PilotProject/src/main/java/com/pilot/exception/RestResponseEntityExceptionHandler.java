/*
 * Id: RestResponseEntityExceptionHandler.java, 11:05:00 AM
 *
 * Copyright (c) 2017
 * All rights reserved.
 *
 */
package com.pilot.exception;

import org.apache.log4j.Logger;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * @author hungpham
 *
 */
@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

	private static final Logger LOG = Logger.getLogger(RestResponseEntityExceptionHandler.class);

	private static final String INTERNAL_ERROR_MESSAGE = "Please contact your administrator.";

	@Override
	protected ResponseEntity<Object> handleExceptionInternal(Exception ex, Object body, HttpHeaders headers,
			HttpStatus status, WebRequest request) {

		LOG.error(ex.getMessage(), ex);

		ErrorResponse error = new ErrorResponse();
		error.setError(ex.getClass().getSimpleName());
		error.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
		error.setMessage(INTERNAL_ERROR_MESSAGE);

		return new ResponseEntity<>(error, HttpStatus.OK);
	}

	/**
	 * Handle Exception.
	 * 
	 * @param ex
	 *            Exception object
	 * @return
	 * @author hungpham
	 */
	@ExceptionHandler(Exception.class)
	@ResponseBody
	public ResponseEntity<Object> handlePilotException(Exception ex) {

		LOG.error(ex.getMessage(), ex);

		ErrorResponse error = new ErrorResponse();
		error.setError(ex.getClass().getSimpleName());
		error.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
		error.setMessage(INTERNAL_ERROR_MESSAGE);
		return new ResponseEntity<>(error, HttpStatus.valueOf(error.getStatus()));
	}

	/**
	 * Handle PilotException.
	 * 
	 * @param ex
	 *            PilotException object
	 * @return
	 * @author hungpham
	 */
	@ExceptionHandler(PilotException.class)
	@ResponseBody
	public ErrorResponse handlePilotException(PilotException ex) {

		LOG.error(ex.getMessage(), ex);

		int status = HttpStatus.INTERNAL_SERVER_ERROR.value();
		if (ex.getErrorCode() > 0) {
			status = ex.getErrorCode();
		}

		ErrorResponse error = new ErrorResponse();
		error.setStatus(status);
		error.setError(ex.getClass().getSimpleName());
		error.setMessages(ex.getErrorMessages());

		return error;
	}

}
