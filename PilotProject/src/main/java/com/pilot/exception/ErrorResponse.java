/*
 * Id: ErrorResponse.java, 11:26:17 AM
 *
 * Copyright (c) 2017
 * All rights reserved.
 *
 */
package com.pilot.exception;

import java.io.Serializable;
import java.util.List;

/**
 * @author hungpham
 *
 */
public class ErrorResponse implements Serializable {

	/**
	 * serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;
	private int status;
	private String error;
	private String message;
	private List<String> messages;

	public int getStatus() {
		return status;
	}

	public void setStatus(int errorCode) {
		this.status = errorCode;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public List<String> getMessages() {
		return messages;
	}

	public void setMessages(List<String> messages) {
		this.messages = messages;
	}

}
