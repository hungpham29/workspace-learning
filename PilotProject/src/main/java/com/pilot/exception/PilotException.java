/*
 * Id: PilotException.java, 11:04:41 AM
 *
 * Copyright (c) 2017
 * All rights reserved.
 *
 */
package com.pilot.exception;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;

import com.pilot.utils.Helper;

/**
 * @author hungpham
 *
 */
public class PilotException extends Exception {

	/**
	 * serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;

	public static final int DATA_ERROR = 1000;

	public static final int DUPLICATE_USERNAME = 1001;

	public static final int ACCOUNT_NOT_EXIST = 1002;

	public static final int DATA_ACCESS_EXCEPTION = 1003;

	public static final int DEFAULT_ADMIN_EXCEPTION = 1004;

	private int errorCode;
	private List<String> errorMessages;

	/**
	 * Constructor.
	 */
	public PilotException() {
		// Default constructor.
	}

	/**
	 * Constructor.
	 */
	public PilotException(int code, String message) {
		super(message);
		this.errorCode = code;
		this.errorMessages = Arrays.asList(message);
	}

	public PilotException(String message) {
		super(message);
		this.errorCode = 0;
		this.errorMessages = Arrays.asList(message);
	}

	public PilotException(int code, List<String> messages) {
		this.errorCode = code;
		this.errorMessages = messages;
	}

	public PilotException(int code, BindingResult errors) {
		this.errorCode = code;
		if (!Helper.isEmpty(errors)) {
			this.errorMessages = errors.getAllErrors().stream().map(ObjectError::getDefaultMessage)
					.collect(Collectors.toList());
		}
	}

	public int getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}

	public List<String> getErrorMessages() {
		return errorMessages;
	}

	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}

}
