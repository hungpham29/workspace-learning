/*
 * Id: Constants.java, 3:04:06 PM
 *
 * Copyright (c) 2017
 * All rights reserved.
 *
 */
package com.pilot.utils;

/**
 * @author hungpham
 *
 */
public final class Constants {

	public static final String EMPTY_STRING = "";

	public static final String SPACE_STRING = " ";

	public static final String PREFIX_ROLE = "ROLE_";

	/**
	 * Private constructor.
	 */
	private Constants() {
	}
}
