/*
 * Id: Helper.java, 5:12:30 PM
 *
 * Copyright (c) 2017
 * All rights reserved.
 *
 */
package com.pilot.utils;

import java.util.Date;
import java.util.List;

/**
 * @author hungpham
 *
 */
public final class Helper {

	/**
	 * Constructor.
	 */
	private Helper() {
	}

	/**
	 * Check list is empty.
	 * 
	 * @author hungpham
	 */
	public static final <T> boolean isEmpty(List<T> list) {
		return list == null || list.isEmpty();
	}

	/**
	 * Check string is empty.
	 * 
	 * @author hungpham
	 */
	public static final boolean isEmpty(String str) {
		return str == null || Constants.EMPTY_STRING.equals(str);
	}

	/**
	 * Check object is empty.
	 * 
	 * @author hungpham
	 */
	public static final boolean isEmpty(Object obj) {
		return obj == null;
	}

	/**
	 * Check list date is empty.
	 * 
	 * @param dates
	 * @return
	 * @author hungpham
	 */
	public static final boolean isEmpty(Date... dates) {
		boolean result = false;

		for (Date date : dates) {
			if (Helper.isEmpty(date)) {
				result = true;
				break;
			}
		}

		return result;
	}

	/**
	 * Check array string is empty.
	 * 
	 * @param strs
	 * @return
	 * @author hungpham
	 */
	public static final boolean isEmpty(String... strs) {
		boolean result = false;

		for (String str : strs) {
			if (Helper.isEmpty(str)) {
				result = true;
				break;
			}
		}

		return result;
	}

	/**
	 * Check array string is not empty.
	 * 
	 * @param strs
	 * @return
	 * @author hungpham
	 */
	public static final boolean oneParamNotEmpty(String... strs) {
		boolean result = false;

		for (String str : strs) {
			if (!Helper.isEmpty(str)) {
				result = true;
				break;
			}
		}

		return result;
	}
}
