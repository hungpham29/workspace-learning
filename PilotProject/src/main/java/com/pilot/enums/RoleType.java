/*
 * Id: Role.java, 4:12:20 PM
 *
 * Copyright (c) 2017
 * All rights reserved.
 *
 */
package com.pilot.enums;

/**
 * @author hungpham
 *
 */
public enum RoleType {
	ADMIN, MEM, SUPRT
}
