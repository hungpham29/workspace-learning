/*
 * Id: Gender.java, 9:06:33 AM
 *
 * Copyright (c) 2017
 * All rights reserved.
 *
 */
package com.pilot.enums;

/**
 * @author hungpham
 *
 */
public enum Gender {
	MALE, FEMALE
}
