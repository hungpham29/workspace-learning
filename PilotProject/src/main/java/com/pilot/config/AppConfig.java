/*
 * Id: AppConfig.java, 10:27:41 AM
 *
 * Copyright (c) 2017
 * All rights reserved.
 *
 */
package com.pilot.config;

import javax.persistence.EntityManagerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.validation.Validator;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * @author hungpham
 *
 */
@Configuration
@EnableTransactionManagement
public class AppConfig extends WebMvcConfigurerAdapter {

	@Autowired
	private MessageSource messageSource;

	@Value("${origin.url}")
	private String originUrl;

	/**
	 * Create validation factory bean.
	 * 
	 * @return LocalValidatorFactoryBean
	 * @author hungpham
	 */
	@Bean
	public LocalValidatorFactoryBean validator() {
		LocalValidatorFactoryBean bean = new LocalValidatorFactoryBean();
		bean.setValidationMessageSource(this.messageSource);
		return bean;
	}

	@Override
	public Validator getValidator() {
		return this.validator();
	}

	/**
	 * Create transaction manager bean.
	 * 
	 * @param entityManagerFactory
	 *            EntityManagerFactory
	 * @return JpaTransactionManager
	 * @author hungpham
	 */
	@Bean
	public JpaTransactionManager transactionManager(EntityManagerFactory entityManagerFactory) {
		JpaTransactionManager transactionManager = new JpaTransactionManager();
		transactionManager.setEntityManagerFactory(entityManagerFactory);
		return transactionManager;
	}

	@Override
	public void addCorsMappings(CorsRegistry registry) {
		registry.addMapping("/api/**").allowedOrigins(originUrl).allowedMethods("*").allowedHeaders("*");
	}
}
